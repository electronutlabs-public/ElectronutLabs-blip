![](./Zephyr-logo.png)

> The Zephyr™ Project is a scalable real-time operating system (RTOS)
supporting multiple hardware architectures, optimized for resource constrained
devices, and built with safety and security in mind.

## Introduction

This guide is to get you quickly started with using Zephyr with blip. We will
not replicate install instructions here, since latest ones are always on the
Zephyr documentation site.

Blip is supported in Zephyr upstream code, however, we do need to mention
one thing, some of the I2C sensor drivers are not yet available in the master
branch of zephyr. So you have two alternatives to installation:

1. [Latest getting started guide](https://docs.zephyrproject.org/latest/getting_started/index.html) - SI7006 and LTR-329ALS-01 drivers not in master branch.
2. Our branch - Based off latest stable release, follow these instructions and continue - [Zephyr(v1.14.0) getting started](https://docs.zephyrproject.org/1.14.0/getting_started/index.html)

> __NOTE__: Following instructions are for if you choose out branch for developing with
Zephyr, if you choose to develop on master branch, you can skip ahead to the
next section.

This branch is based off `Zephyr` release v1.14, we only added drivers code for
which is not yet in mainline zephyr (it's in the process of being merged). To
use this branch, you will need Zephyr SDK v0.10.0.

Once you have followed install instructions for V1.14.0, open a terminal and
run the following commands:

```
cd /path/to/zephyr/
git remote add electronut https://github.com/electronut/zephyr.git
git fetch --all
git checkout electronut/blip_dev
```

## Running a BLE sample

Open up a terminal, connect Blip to your computer with a microUSB cable on
the bedugger USB port.

```bash
$ cd /path/to/zephyr/
$ . zephyr-env.sh
$ cd samples/bluetooth/peripheral_hr/
$ mkdir build
$ cd build
$ cmake -GNinja -DBOARD=nrf52840_blip ..
...
...
...
-- Build files have been written to: /home/tavish/repos/zephyrproject/zephyr/samples/bluetooth/peripheral_hr/build
$ ninja
[0/1] Re-running CMake...
Zephyr version: 1.14.0
-- Selected BOARD nrf52840_blip
...
...
...
-- Build files have been written to: /home/tavish/repos/zephyrproject/zephyr/samples/bluetooth/peripheral_hr/build
[156/161] Linking C executable zephyr/zephyr_prebuilt.elf
Memory region         Used Size  Region Size  %age Used
           FLASH:      120179 B         1 MB     11.46%
            SRAM:       19996 B       256 KB      7.63%
        IDT_LIST:         120 B         2 KB      5.86%
[161/161] Linking C executable zephyr/zephyr.elf
$ ninja flash
```

Press reset after `ninja flash`, and open a phone app, like nRF connect for
example to see your board advertising as a heart rate monitor device sending
out fake heart rate data!

## Building and running the default firmware

Blip ships with a firmware based on Zephyr to show its sensor values on to our
Electronut Labs android and iOS apps. You can even use it on your browser
directly using webbluetooth, no need to install anything!

More details on this are on the page for [Blip default firmware](code/default_fw).

## Zephyr resources

* [Zephyr project](https://www.zephyrproject.org/)
* [Zephyr documentation](http://docs.zephyrproject.org/index.html)
