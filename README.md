![blip](blip.jpg)

## What is Blip?

Blip is a development board for Bluetooth Low Energy (BLE) and 802.15.4 based wireless applications, based on the Nordic Semiconductor nRF52840 SoC. It has a Black Magic Probe compatible programmer and debugger built in, along with temperature/humidity sensor, ambient light intensity sensor, and a 3-axis accelerometer. It can be used to prototype very low power devices. It also has provision for an SD card slot, which makes it a complete and versatile development board.

## Purchasing Blip

Blip is currently in the process of crowdfunding production - see our [Crowd Supply](https://www.crowdsupply.com/electronut-labs/blip/crowdfunding) page!

Currently you can pre-order Blip by visiting this page - [Blip pre-order](https://electronut.in/products/blip/). Please email us at **info@electronut.in** if you have any questions.

## Hardware Specifications

* Raytac MDBT50Q-1M module based on Nordic Semiconductor's nRF52840
* LIS2DDH12 High-performance 3-axis "femto" accelerometer 
* Optical Sensor LTR-329ALS-01
* Si7006-A20 I2C humidity and temperature sensor
* On board STM32F103CBT6 as Black magic probe debugger
* NFC Antenna
* MicroSD slot
* Power Supply: USB, JST connector for Li-ion/Li-po
* BQ24079 battery charging and power management IC

## Getting Started

Blip allows you to do many things with the on-board temperature, humidity,
accelerometer and light sensors. Here's how you can get started with blip
using the [Electronut Labs mobile app](#default-firmware-with-app):

### Hardware Setup

Blip can be powered in three ways, either using USB, battery connector, or using the VEXT header on the board. Blip has a power selection switch to select power supply option, select VREG for USB and battery connector, VEXT headers for external power source. 

* In VREG position, Blip is powered by output of on-board 3.3V regulator. (Vdd = Vreg)

* In VEXT position, Blip is directly powered from voltage at VEXT pin. (Vdd = Vext)
[Warning: VEXT must not exceed 3.3V]

**Maximum Input Voltage**

* USB powered : 5V

* Vext = 1.8V to 3.3V

* Vbat = Lithium ion/polymer 3.7/4.2V battery

**Note**

**Power Rating** 

* Max battery charging current : 150mA

* When powering with Vext, the debugger, charging IC, and LDO are bypassed for low current consumption

### Default Firmware with App

Blip repositories have a firmware that works with our app. Please follow these
instructions to program this firmware from [Build-and-flash-firmware](code/default_fw/README.md#build-and-flash-firmware).

Once programmed, as soon as you power blip, the firmware will start advertising.
The data can be seen on Electronut Labs mobile app after connecting to the device.

The mobile app can be downloaded from the link below.

* [Android app](https://play.google.com/store/apps/details?id=in.electronut.app&hl=en_US) for blip.

The below contains three screenshots of the Electronut Labs App. 

![blip_app](blip_app.png)

For more details about default firmware, visit *demos & examples* section in this docs.

For more details about the default firmware and Electronut Labs app, please see our page on [default firmware](code/default_fw/).

## Datasheet and schematic

You can find the schematic and datasheet in the links below:

* <i class="fa fa-file fa-1x" style="color: black"></i> [Datasheet](blip_datasheet.pdf)
* <i class="fa fa-file fa-1x" style="color: black"></i> [Schematic](blip_v0.3_schematic.pdf)

## Blip Dimensions

![dimesions](blip_board_dimensions.png)

![componentmarkings](blip_components_marking.png)

## Repository

You can find sample firmware, datasheet, images etc. on its <i class="fa fa-git fa-1x" style="color: black"></i> [repository](https://gitlab.com/electronutlabs-public/ElectronutLabs-blip).

## About Electronut Labs

**Electronut Labs** is an Embedded Systems company based in Bangalore, India. More information at our [website](https://electronut.in).
