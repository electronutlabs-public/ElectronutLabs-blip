Blip GPS shield
===================

![](gps_shield1.jpg)

[Purchase from here](https://www.crowdsupply.com/electronut-labs/blip)

This shield adds a GPS module

## GPS module details

* Model: GDEW0154Z04
* Resolution: 200 X 200
* Screen size: 1.54 Inch, active area: 27.6mm X 27.6mm

## Resources

* <i class="fa fa-file fa-1x" style="color: black"></i> [Schematic](Schematic-GPS_shield.pdf)
* <i class="fa fa-git fa-1x" style="color: black"></i>  [Blip repository](https://gitlab.com/electronutlabs-public/ElectronutLabs-blip)