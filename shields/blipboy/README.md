Blipboy shield
==============

![](blipboy1.jpg)

This shield can be plugged on to Blip to convert it into a handheld
gaming device!

Blip + blipboy shield support CircuitPython, Arduino, Zephyr, etc. You
can play (and make) games in python for this, we already one sample
running, more to come.

## Features

* Display: ILI9163 based 160x128, 1.8" TFT LCD
* Buttons: 5-way button, 4 push buttons
* Audio: PAM8301 audio amp with 8 Ohm speaker

## Downloads

* <i class="fa fa-file fa-1x" style="color: black"></i> [Schematic](schematic-blipboy.pdf)