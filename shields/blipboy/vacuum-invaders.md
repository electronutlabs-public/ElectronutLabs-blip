
We have added support for CircuitPython on blip with [blip-boy shield](https://docs.electronut.in/blip/shields/blipboy/) . Now you can easily develop any firmware in python using blip. Here we will show how you can run the **classic Space Invaders arcade game** on blip using circuit python.  

The code for his game is taken from **[python-ugame/vacuum-invaders](https://github.com/python-ugame/vacuum-invaders)** repository on github by Radomir Dopieralski. We would like to thank him for it, it made our task easier.

![Vacuum Invaders!](./vacuum.jpg)

## Instruction to run Vacuum Invaders on Blip Boy

1. Download [Nordic semicondictor's s140 softdevice](code/s140/s140_nrf52_6.1.0_softdevice.hex)
2. Download Circuitpython firmware `.hex` file, at least 5.x from [circuitpython.org Blip page](https://circuitpython.org/board/electronut_labs_blip/). Note: last tested working with [5.0.0-alpha.5](https://adafruit-circuit-python.s3.amazonaws.com/bin/electronut_labs_blip/en_US/adafruit-circuitpython-electronut_labs_blip-en_US-5.0.0-alpha.5.hex)
3. Flash Circuitpython to Blip

```bash
tavish@computer:~/repos/el/public/ElectronutLabs-blip (master)*$ arm-none-eabi-gdb
GNU gdb (GNU Tools for Arm Embedded Processors 7-2018-q3-update) 8.1.0.20180315-git
Copyright (C) 2018 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
(gdb) tar ext /dev/ttyACM0
Remote debugging using /dev/ttyACM0
warning: No executable has been specified and target does not support
determining executable automatically.  Try using the "file" command.
0x0000384c in ?? ()
(gdb) mon swdp_scan
Target voltage: unknown
Available Targets:
No. Att Driver
 1      Nordic nRF52
 2      Nordic nRF52 Access Port
(gdb) att 1
A program is being debugged already.  Kill it? (y or n) y
Attaching to Remote target
warning: No executable has been specified and target does not support
determining executable automatically.  Try using the "file" command.
0x0000384c in ?? ()
(gdb) load s140_nrf52_6.1.0_softdevice.hex
...
...
(gdb) load adafruit-circuitpython-electronut_labs_blip-en_US-20191014-756e4a4.hex
...
...
```
Now the blip will appear as CIRCUITPY drive in your system. 

4. Clone [ElectronutLabs-blip](https://gitlab.com/electronutlabs-public/ElectronutLabs-blip.git) repository and copy the contents of `code/shields/blip_boy_vacumm_invaders_game` directory and paste the content to CIRCUITPY folder when you plug in the Blip.

Now, Vacuum invaders, created by [Radomir Dopieralski](https://github.com/deshipu) will start playing on your blip!

If you any trouble with these steps, we have a single hex file (which inclues the softdevice, circuitpython 5.0.0-alpha.5 and the vacuum invaders code) available as a `.hex` file, that can be flashed in one go. It can be found in the [code/circuitpython_hex](code/circuitpython_hex/blip_vacuum_invaders_combined.hex). Just erase blip and flash this one `.hex` file, and you can still run Vacuum Invaders.
