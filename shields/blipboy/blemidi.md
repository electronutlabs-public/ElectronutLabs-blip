# MIDI sequencer with blipboy shield

![](./synth.jpg)

This is an Arduino project you can do with Blip+Blipboy shield to make a tiny
MIDI sequencer which works over BLE. MIDI protocol is a 'serial' protocol. BLE
specifications have an offical MIDI transport spec too, which means you can make
wireless MIDI devices!

We'll draw a 8x8 grid on Blpboy's LCD screen. Each row represents a MIDI note,
For now, we'll fix them to a C major scale, 8 notes, i.e. a full octave. The
sequencer code has a fixed tempo, and at every beat, it will send out a not in
the current column (active ones are filled white, inactive ones empty). So
that's how our basic sequencer will work.

In this project we will do 3 main things:

- Draw a grid for the sequencer, also show current column, current edit box, and
  ofcourse the boxes in the grid
- We have a total of 9 input buttons possible (4 + 5-way joystick), that is used
  to move the edit box, and to toggle state of a box
- Send out notes as MIDI messages as BLE MIDI notifications

For drawing utility functions, we will use
[Adafruit's GFX](https://github.com/adafruit/Adafruit-GFX-Library) graphics
library for Arduino, and for our specific LCD we'll use our [ILI9163 library](https://github.com/electronut/Adafruit_ILI9163).

For MIDI, we'll need Francois Best's [MIDI library](https://github.com/FortySevenEffects/arduino_midi_library).
Bluefruit library from Adafruit is already present inside Arduino core for Blip,
so that does not need installing.

To make button input easy, we'll use [Button library](https://github.com/tigoe/Button).

The source code can be found at:

* <i class="fa fa-git fa-1x" style="color: black"></i> [sequencer.ino](../../code/shields/ble-midi-sequencer/sequencer.ino)

To connect and listen to what is being played, you can try the following two
apps:

1. [MIDI BLE Connect](https://play.google.com/store/apps/details?id=com.mobileer.example.midibtlepairing)
2. [Fluidsynth MIDI Synthesizer](https://play.google.com/store/apps/details?id=net.volcanomobile.fluidsynthmidi), or any other compatible one also like [Synthesia](https://play.google.com/store/apps/details?id=com.synthesia.synthesia)

## References

* https://learn.adafruit.com/wireless-untztrument-using-ble-midi/android
* https://newt.phys.unsw.edu.au/jw/notes.html