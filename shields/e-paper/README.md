Blip e-paper shield
===================

![](epaper_shield1.jpg)

This shield can be plugged on to Blip to convert it into
a low power display device. It uses the same 1.54" display
that is on our other product.

## E-paper details

* Model: GDEW0154Z04
* Resolution: 200 X 200
* Screen size: 1.54 Inch, active area: 27.6mm X 27.6mm

## Downloads

* <i class="fa fa-file fa-1x" style="color: black"></i> [Schematic](e-paper_shield.pdf)