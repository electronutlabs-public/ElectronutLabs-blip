#ifndef __COMMON_H__
#define __COMMON_H__

/* shared output: contains raw accel values*/
// extern s32_t raw_output_data[32][3];
extern double f_raw_output_data[32][3];
extern const k_tid_t fifo_read_id;

extern struct k_msgq logging_q;
extern uint8_t data_ready, button_pressed;

#define EVT_BUTTON_ENABLE_LOG 	0x11
#define EVT_BUTTON_DISBLE_LOG 	0x22
#define EVT_DATA_READY		    0x33

#endif // __COMMON_H__
