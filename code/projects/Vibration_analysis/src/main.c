/*
 * Copyright (c) Electronut Labs June, 2019
 *
 * main.c for Vibration Analysis project
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <disk_access.h>
#include <logging/log.h>
#include <fs.h>
#include <ff.h>
#include <gpio.h>
#include <device.h>
#include <stdio.h>
#include "lis2dh12_fifo.h"
#include "common.h"

#define LOG_LEVEL LOG_LEVEL_WARN
#include <logging/log.h>
LOG_MODULE_REGISTER(VIBRATION);

#define GPIO_PORT "GPIO_1"
#define BUTTON_NUMBER 7
#define LED_PORT LED0_GPIO_CONTROLLER
#define LED	LED0_GPIO_PIN

static struct device *gpio_dev;
static struct gpio_callback gpio_cb;
static bool flag_logging_toggle = false;

static struct fs_file_t file;
static const char *disk_mount_pt = "/SD:";

K_MSGQ_DEFINE(logging_q, 1, 1, 1);
uint8_t data_ready, button_pressed;

static FATFS fat_fs;
/* mounting info */
static struct fs_mount_t mp = {
	.type = FS_FATFS,
	.fs_data = &fat_fs,
};

static void button_interrupt_callback(struct device *dev,
									  struct gpio_callback *cb, u32_t pins)
{
	if (flag_logging_toggle == false)
	{
		flag_logging_toggle = true;
		button_pressed = EVT_BUTTON_ENABLE_LOG;
		while (k_msgq_put(&logging_q, &button_pressed, K_NO_WAIT) != 0)
		{
			/* message queue is full: purge old data & try again */
			k_msgq_purge(&logging_q);
		}
	}
	else
	{
		flag_logging_toggle = false;
		button_pressed = EVT_BUTTON_DISBLE_LOG;
		while (k_msgq_put(&logging_q, &button_pressed, K_NO_WAIT) != 0)
		{
			/* message queue is full: purge old data & try again */
			k_msgq_purge(&logging_q);
		}
	}
}

void main(void)
{
	printk("Hello World! %s\n", CONFIG_BOARD);

	struct device *dev;

	dev = device_get_binding(LED_PORT);
	/* Set LED pin as output */
	gpio_pin_configure(dev, LED, GPIO_DIR_OUT);
	gpio_pin_write(dev, LED, 1);

	k_sleep(2000);

	/* initialise LIS sensor */
	lis2dh12_init();
	k_sleep(50);
	lis2dh12_config();
	k_sleep(50);

	/* gpio initialisation*/
	gpio_dev = device_get_binding(GPIO_PORT);

	if (gpio_dev == NULL)
	{
		printk("Failed to get pointer device!");
	}

	gpio_pin_configure(gpio_dev, BUTTON_NUMBER,
					   GPIO_DIR_IN | GPIO_INT | GPIO_PUD_PULL_UP | GPIO_INT_EDGE);

	gpio_init_callback(&gpio_cb,
					   button_interrupt_callback,
					   BIT(BUTTON_NUMBER));

	if (gpio_add_callback(gpio_dev, &gpio_cb) < 0)
	{
		printk("Failed to set gpio callback!");
	}

	gpio_pin_enable_callback(gpio_dev, BUTTON_NUMBER);

	k_thread_start(fifo_read_id);

	uint8_t val;
	char out_str[100];
	int written;
	bool logging_enabled = false;
	bool mounted = false;

	while (1)
	{
		//printk("%s",out_str);
		k_msgq_get(&logging_q, &val, K_FOREVER);

		switch (val)
		{
		case EVT_BUTTON_ENABLE_LOG:
			// mount and open file here
			mp.mnt_point = disk_mount_pt;
			int res = fs_mount(&mp);

			gpio_pin_write(dev, LED, 0);

			if (res == FR_OK) {
				LOG_DBG("Disk mounted.\n");
				mounted = true;
			}
			else {
				LOG_ERR("Error mounting disk.\n");
				mounted = false;
			}

			int rc = fs_open(&file, "/SD:/myfile.csv");
			if (rc != 0) {
				LOG_ERR("file not created\n");
				mounted = false;
			}
			logging_enabled = true;

			printk("logging enabled\r\n");
			break;

		case EVT_BUTTON_DISBLE_LOG:
			// close file and unmount here
			gpio_pin_write(dev, LED, 1);
			mounted = false;
			if (fs_close(&file) != 0)
			{
				LOG_ERR("file not closed");
			}
			if (fs_unmount(&mp) != 0)
			{
				LOG_ERR("Unable to unmount");
			}
			logging_enabled = false;
			printk("logging disabled\r\n");
			break;

		case EVT_DATA_READY:
			if (logging_enabled && mounted)
			{
				// write to file, dont open/mount, and dont' close/unmount
				for (int i = 0; i < 32; i++)
				{
					written = snprintf(out_str, sizeof(out_str) - 1, "%.6lf, %.6lf, %.6lf\r\n",
									   f_raw_output_data[i][0],
									   f_raw_output_data[i][1],
									   f_raw_output_data[i][2]);

					int w_size = fs_write(&file, out_str, written);

					if (w_size > 0)
					{
						LOG_DBG("%d bytes written\r\n", w_size);
					}
					else
					{
						LOG_ERR("err in writing");
					}
				}
			}
			break;
		default:
			break;
		}
	}
}
