/** @file LIS2DH12.h
 *
 * @brief Driver for LIS2DH12 chip
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Electronut Labs.
 * All rights reserved. 
*/

#ifndef __LIS2DH12_H__
#define __LIS2DH12_H__

#include <stdint.h>
#include <stdbool.h>

#include "admiral_common.h"

/*Pin Definitions*/
// #define SA0                     14
// #define CS                      13
#define INT1                            LIS2DH12_INT1_PIN
#define INT2                            LIS2DH12_INT2_PIN

/*Device Address*/
#define LIS2DH12_ADDR                   0x19U     // SA0 HIGH.
// #define LIS2DH12_ADDR                   0x18U     // SA0 LOW.

#define LIS2DH12_FIFO_DEPTH             32

extern uint16_t fifo_x[];
extern uint16_t fifo_y[];
extern uint16_t fifo_z[];

// these two enums are used to figure out scaling
typedef enum {
  LIS2DH12_FS_2G,
  LIS2DH12_FS_4G,
  LIS2DH12_FS_8G,
  LIS2DH12_FS_16G,
} LIS2DH12_FS_t;

typedef enum {
  LIS2DH12_CURR_OP_MODE_LP,
  LIS2DH12_CURR_OP_MODE_NRML,
  LIS2DH12_CURR_OP_MODE_HR,
} LIS2DH12_CURR_OP_MODE_t;

struct LIS2DH12_settings {
  LIS2DH12_CURR_OP_MODE_t curr_mode;
  LIS2DH12_FS_t full_scale;
  float factor_mg;
};

// ODR values
#define REG1_ODR_1_HZ                   0x10
#define REG1_ODR_10_HZ                  0x20
#define REG1_ODR_25_HZ                  0x30
#define REG1_ODR_50_HZ                  0x40
#define REG1_ODR_100_HZ                 0x50
#define REG1_ODR_200_HZ                 0x60
#define REG1_ODR_400_HZ                 0x70

// holds current settings
struct LIS2DH12_settings accel_settings;

/*
 * function to test availablility of accelerometer
*/
uint8_t LIS2DH12_who_am_i(void);

/**
 * @brief initialize sensor settings etc.
 * 
 * @param threshold in steps of 32mg, up to 0x7F
 * @param duration in steps of 1/25 s, up to 0x7F
 */
void LIS2DH12_init(uint8_t threshold, uint8_t duration);

/**
 * @brief writes current to chip
 * 
 */
void LIS2DH12_write_config();

/**
 * @brief Get the factor for scaling accel values to g, uses accel_settings.
 * Used to set accel_settings.factor_mg after changing settings.
 * 
 * @return float 
 */
float get_factor();

/**
 * @brief read complete FIFO
 * 
 * @param gx float buffer, has to be of LIS2DH12_FIFO_DEPTH size
 * @param gy float buffer, has to be of LIS2DH12_FIFO_DEPTH size
 * @param gz float buffer, has to be of LIS2DH12_FIFO_DEPTH size
 * @return number of elements read, -1 on error
 */
int LIS2DH12_fifo_read(float *gx, float *gy, float *gz);

/*
 * function to read available accelerometer data
*/
void LIS2DH12_read_acc_data(int16_t *x_value, int16_t *y_value, int16_t *z_value);

/**
 * @brief function to convert ADC data to acceleromenter data in g.
 */
static inline float get_accel_data_in_g(int16_t val)
{
  return (val * (accel_settings.factor_mg / 1000.0f)); // convert factor in mg to g and multiply by data.
}

/*
 *  Read (to clear) interrupt status register.
 */
uint8_t LIS2DH12_read_int_status();

/**
 * @brief enables thereshold interrupt on INT1 pin
 * 
 */
void LIS2DH12_threshold_int_enable();

/**
 * @brief disables thereshold interrupt on INT1 pin
 * 
 */
void LIS2DH12_threshold_int_disable();

/**
 * @brief enables DRDY interrupt on INT1 pin
 * 
 */
void LIS2DH12_DRDY_int_enable();

/**
 * @brief disables DRDY int on INT1 pin
 * 
 */
void LIS2DH12_DRDY_int_disable();

/**
 * @brief sets ODR
 * 
 * @param val 
 */
void LIS2DH12_ODR_set(uint8_t val);

#endif