"""
test_fft.py

Testing code for FFT, plotting, etc.

Author: Mahesh Venkitachalam
Website: electronut.in

"""

import numpy as np
import sys
from matplotlib import pyplot
from time import sleep
import argparse

import math 

N = 4096


def showFuncFFT():

    # audio data 
    x = np.arange(N)
    y = 4*np.sin(10*2*math.pi*x/N) + 2.5*np.sin(30*2*math.pi*x/N) 

    # FFT
    fft = np.abs(np.fft.rfft(y))*2.0/N
    #freqs = range(len(fft))
    freqs = np.fft.rfftfreq(y.size, 1/N)

    pyplot.title('Function FFT')

    # plot audio
    pyplot.subplot(2, 1, 1)
    pyplot.plot( x, y, '-' )
    pyplot.xlabel('time')
    pyplot.ylabel('Amplitude')
    
    # plot FFT
    pyplot.subplot(2, 1, 2)
    pyplot.plot(freqs[:100], fft[:100], '-' )
    pyplot.xlabel('frequency')
    pyplot.ylabel('Intensity')
    
    #pyplot.subplots_adjust(bottom=-0.2)

    pyplot.show()


# main() function
def main():
    # use sys.argv if needed
    print('testing FFT...')
    
    #grabAudio('song.bin')
    #showAudioFFT('song.bin')
  
    showFuncFFT()

# call main
if __name__ == '__main__':
    main()
