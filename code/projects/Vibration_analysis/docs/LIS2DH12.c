/** @file LIS2DH12.c
 *
 * @brief Driver for LIS2DH12 chip
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Electronut Labs.
 * All rights reserved. 
*/

#include "admiral_twi.h"

#include "LIS2DH12.h"
#include "admiral_common.h"


/*Register Addresses*/
#define ADDR_WHO_AM_I                        0x0F

#define ADDR_CTRL_REG0                       0x1E
#define ADDR_TEMP_CFG_REG                    0X1F
#define ADDR_CTRL_REG1                       0x20
#define ADDR_CTRL_REG2                       0x21
#define ADDR_CTRL_REG3                       0x22
#define ADDR_CTRL_REG4                       0x23
#define ADDR_CTRL_REG5                       0x24
#define ADDR_CTRL_REG6                       0x25

#define ADDR_REFERENCE                       0x26

#define ADDR_STATUS_REG                      0x27

#define ADDR_OUT_X_L                         0x28
#define ADDR_OUT_X_H                         0x29
#define ADDR_OUT_Y_L                         0x2A
#define ADDR_OUT_Y_H                         0x2B
#define ADDR_OUT_Z_L                         0x2C
#define ADDR_OUT_Z_H                         0x2D

#define ADDR_FIFO_CTRL_REG                   0x2E
#define ADDR_FIFO_SRC_REG                    0x2F

#define ADDR_INT1_CFG                        0x30
#define ADDR_INT1_SRC                        0x31
#define ADDR_INT1_THS                        0x32
#define INT1_DURATION                   0x33
#define ADDR_INT2_CFG                        0x34
#define ADDR_INT2_SRC                        0x35
#define ADDR_INT2_THS                        0x36
#define ADDR_INT2_DURATION                   0x37

#define REG1_LOW_PWR_MODE_DISABLE       (0)
#define REG1_LOW_PWR_MODE_ENABLE        (1<<3)
#define REG1_ENABLE_AXIS_X              0x01
#define REG1_ENABLE_AXIS_Y              0x02
#define REG1_ENABLE_AXIS_Z              0x04
#define REG1_ENABLE_AXIS_XY             0x03
#define REG1_ENABLE_AXIS_YZ             0x06
#define REG1_ENABLE_AXIS_XZ             0x05
#define REG1_ENABLE_AXIS_XYZ            0x07
#define REG1_POWER_DOWN                 0x00
// rest of the values in header
#define REG1_ODR_1_620_KHZ              0x80         // Low-power (1.620 kHz)
#define REG1_ODR_1_344_OR_5_376_KHZ     0x90        // Normal/HR (1.344 kHz), Low-power (5.376 kHz)

#define REG2_HPM_NORMAL                 0 // can (but don't have to) clear DC by reading ADDR_REFERENCE reg
#define REG2_HPM_REFERENCE              (1<<6)
#define REG2_HPM_NORMAL2                (1<<7)
#define REG2_HPM_AUTORESET              ((1<<7) | (1<<6))
#define REG2_FDS_BYPASS                 0
#define REG2_FDS_USE                    (1<<3) // filtered data sent to data regs and FIFO
#define REG2_HP_IA1_ENABLED             (1<<0)
#define REG2_HP_IA2_ENABLED             (1<<1)

// NOTE: always manually change accel_settings.full_scale/odr/curr_mode when you 
// change these settings, not handled automatically, and needed for scaling data

#define LIS2DH12_READ_REGISTER(reg, data, size)  twi_read_reg8(LIS2DH12_ADDR, reg, data, size)
#define LIS2DH12_WRITE_REGISTER(reg, data, size) twi_write_reg8(LIS2DH12_ADDR, reg, data, size)

// to keep track of current state
typedef struct 
{
  uint8_t reg1;
  uint8_t reg2;
  uint8_t reg3;
  uint8_t reg4;
  uint8_t reg5;
  uint8_t reg6;
  uint8_t reference;
  uint8_t int1_ths;
  uint8_t int1_dur;
  uint8_t int1_cfg;
  uint8_t int2_ths;
  uint8_t int2_dur;
  uint8_t int2_cfg;
  uint8_t fifo_ctrl_reg;
}lis2dh12_t;

static lis2dh12_t ctx;

// new config, 1Hz ODR, DRDY on INT1
void LIS2DH12_write_config()
{
  uint8_t reg;
  
  reg = ADDR_CTRL_REG1;
  LIS2DH12_WRITE_REGISTER(reg, &ctx.reg1, sizeof(ctx.reg1));
  
  reg = ADDR_CTRL_REG2; // TODO verify
  LIS2DH12_WRITE_REGISTER(reg, &ctx.reg2, sizeof(ctx.reg2));

  reg = ADDR_CTRL_REG3;
  LIS2DH12_WRITE_REGISTER(reg, &ctx.reg3, sizeof(ctx.reg3));

  reg = ADDR_CTRL_REG4;
  LIS2DH12_WRITE_REGISTER(reg, &ctx.reg4, sizeof(ctx.reg4));

  reg = ADDR_INT1_CFG;
  LIS2DH12_WRITE_REGISTER(reg, &ctx.int1_cfg, sizeof(ctx.int1_cfg));

  reg = ADDR_INT1_THS;
  LIS2DH12_WRITE_REGISTER(reg, &ctx.int1_ths, sizeof(ctx.int1_ths));

  reg = INT1_DURATION;
  LIS2DH12_WRITE_REGISTER(reg, &ctx.int1_dur, sizeof(ctx.int1_dur));
}

void LIS2DH12_init(uint8_t threshold, uint8_t duration)
{
  accel_settings.curr_mode = LIS2DH12_CURR_OP_MODE_NRML;
  accel_settings.full_scale = LIS2DH12_FS_4G;
  accel_settings.factor_mg = get_factor();

  ctx.reg1 = REG1_ODR_25_HZ | REG1_LOW_PWR_MODE_DISABLE | REG1_ENABLE_AXIS_XYZ;
  ctx.reg2 = REG2_HPM_NORMAL | REG2_FDS_BYPASS | REG2_HP_IA1_ENABLED;
  ctx.reg3 = 0;
  ctx.reg4 = (1<<7) | (1<<4);
  ctx.int1_cfg = 0;
  ctx.int1_ths = 0x7f & (threshold); // 0.702g, 1LSB = 32mg @4g FS
  ctx.int1_dur = 0x7F & (duration);  // 0.5s, 1LSB = 1/ODR

  LIS2DH12_threshold_int_enable();

  LIS2DH12_write_config();
}

uint8_t LIS2DH12_who_am_i(void)
{
  uint8_t who_am_i = 0;
  LIS2DH12_READ_REGISTER(ADDR_WHO_AM_I, &who_am_i, 1);
  return who_am_i;
}

uint8_t LIS2DH12_read_int_status()
{
  uint8_t status;
  uint32_t err_code;

  err_code = LIS2DH12_READ_REGISTER(ADDR_INT1_SRC, &status, 
  sizeof(status));
  if(err_code != NRF_SUCCESS)
  {
    status = 0;
  }

  return status;
}

int LIS2DH12_fifo_read(float *gx, float *gy, float *gz)
{
  uint32_t err_code;
  int n_depth = 0;

  // intermediate variables
  uint8_t xL, xH, yL, yH, zL, zH;
  int16_t x_val, y_val, z_val;

  goto finish;

  n_depth = LIS2DH12_FIFO_DEPTH;
  for(int i=0; i<LIS2DH12_FIFO_DEPTH; i++)
  {
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_X_L, &xL, 1);
    if(err_code != NRF_SUCCESS)
    {
      n_depth = -1;
      goto finish;
    }
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_X_H, &xH, 1);
    if(err_code != NRF_SUCCESS)
    {
      n_depth = -1;
      goto finish;
    }
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_Y_L, &yL, 1);
    if(err_code != NRF_SUCCESS)
    {
      n_depth = -1;
      goto finish;
    }
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_Y_H, &yH, 1);
    if(err_code != NRF_SUCCESS)
    {
      n_depth = -1;
      goto finish;
    }
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_Z_L, &zL, 1);
    if(err_code != NRF_SUCCESS)
    {
      n_depth = -1;
      goto finish;
    }
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_Z_H, &zH, 1);
    if(err_code != NRF_SUCCESS)
    {
      n_depth = -1;
      goto finish;
    }

    // combining ADDR_OUT_X_L and ADDR_OUT_X_H data in a single variable.
    x_val = (xH << 8) | xL;
    y_val = (yH << 8) | yL;
    z_val = (zH << 8) | zL;

    // data from accelerometer is left justified by default. Bit manipulation for valid data.
    switch (accel_settings.curr_mode)
    {
    case LIS2DH12_CURR_OP_MODE_LP: // 8-bit data
      fifo_x[i] = (x_val >> 8);
      fifo_y[i] = (y_val >> 8);
      fifo_z[i] = (z_val >> 8);
      break;

    case LIS2DH12_CURR_OP_MODE_NRML: // 10-bit data
      fifo_x[i] = (x_val >> 6);
      fifo_y[i] = (y_val >> 6);
      fifo_z[i] = (z_val >> 6);
      break;

    case LIS2DH12_CURR_OP_MODE_HR: // 12-bit data
      fifo_x[i] = (x_val >> 4);
      fifo_y[i] = (y_val >> 4);
      fifo_z[i] = (z_val >> 4);
      break;
    }
  }

finish:
  return n_depth;
}

void LIS2DH12_read_acc_data(int16_t *x_value, int16_t *y_value, int16_t *z_value)
{
  ret_code_t err_code;
  uint8_t xL, xH, yL, yH, zL, zH;
  int16_t x_val, y_val, z_val;
  uint8_t status = 0;

  // check if bit 3 in ADDR_STATUS_REG is set. If true, new data available.
  do
  {
    err_code = LIS2DH12_READ_REGISTER(ADDR_STATUS_REG, &status, sizeof(status));
    if(err_code)
    {
      goto finish;
    }
  } while (!(status && 0b00001000));

  // check if bit 7 in ADDR_STATUS_REG is set. If true, read the latest data.
  if (status && 0b10000000)
  {
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_X_L, &xL, 1);
    if(err_code)
    {
      goto finish;
    }
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_X_H, &xH, 1);
    if(err_code)
    {
      goto finish;
    }
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_Y_L, &yL, 1);
    if(err_code)
    {
      goto finish;
    }
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_Y_H, &yH, 1);
    if(err_code)
    {
      goto finish;
    }
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_Z_L, &zL, 1);
    if(err_code)
    {
      goto finish;
    }
    err_code = LIS2DH12_READ_REGISTER(ADDR_OUT_Z_H, &zH, 1);
    if(err_code)
    {
      goto finish;
    }
  }

  // combining ADDR_OUT_X_L and ADDR_OUT_X_H data in a single variable.
  x_val = (xH << 8) | xL;
  y_val = (yH << 8) | yL;
  z_val = (zH << 8) | zL;

  // data from accelerometer is left justified by default. Bit manipulation for valid data.
  switch (accel_settings.curr_mode)
  {
  case LIS2DH12_CURR_OP_MODE_LP: // 8-bit data
    *x_value = (x_val >> 8);
    *y_value = (y_val >> 8);
    *z_value = (z_val >> 8);
    break;

  case LIS2DH12_CURR_OP_MODE_NRML: // 10-bit data
    *x_value = (x_val >> 6);
    *y_value = (y_val >> 6);
    *z_value = (z_val >> 6);
    break;

  case LIS2DH12_CURR_OP_MODE_HR: // 12-bit data
    *x_value = (x_val >> 4);
    *y_value = (y_val >> 4);
    *z_value = (z_val >> 4);
    break;
  }

finish:
  return;
}

float get_factor()
{
  float factor = 4; // in mg
  switch (accel_settings.full_scale)
  {
  case LIS2DH12_FS_2G: // Full scale = 2.0g
    switch (accel_settings.curr_mode)
    {
    case LIS2DH12_CURR_OP_MODE_LP: // 8-bit
      factor = 16;
      break;

    case LIS2DH12_CURR_OP_MODE_NRML: // 10-bit
      factor = 4;
      break;

    case LIS2DH12_CURR_OP_MODE_HR: // 12-bit
      factor = 1;
      break;
    }
    break;

  case LIS2DH12_FS_4G: // Full scale = 4.0g
    switch (accel_settings.curr_mode)
    {
    case LIS2DH12_CURR_OP_MODE_LP: // 8-bit
      factor = 32;
      break;

    case LIS2DH12_CURR_OP_MODE_NRML: // 10-bit
      factor = 8;
      break;

    case LIS2DH12_CURR_OP_MODE_HR: // 12-bit
      factor = 2;
      break;
    }
    break;

  case LIS2DH12_FS_8G: // Full scale = 8.0g
    switch (accel_settings.curr_mode)
    {
    case LIS2DH12_CURR_OP_MODE_LP: // 8-bit
      factor = 64;
      break;

    case LIS2DH12_CURR_OP_MODE_NRML: // 10-bit
      factor = 16;
      break;

    case LIS2DH12_CURR_OP_MODE_HR: // 12-bit
      factor = 4;
      break;
    }
    break;

  case LIS2DH12_FS_16G: // Full scale = 16.0g
    switch (accel_settings.curr_mode)
    {
    case LIS2DH12_CURR_OP_MODE_LP: // 8-bit
      factor = 192;
      break;

    case LIS2DH12_CURR_OP_MODE_NRML: // 10-bit
      factor = 48;
      break;

    case LIS2DH12_CURR_OP_MODE_HR: // 12-bit
      factor = 12;
      break;
    }
    break;
  }

  return factor;
}

void LIS2DH12_threshold_int_enable()
{
  ctx.reg3 |= (1<<6);
  ctx.int1_cfg = (1<<1) | (1<<3) | (1<<5);
}

void LIS2DH12_threshold_int_disable()
{
  ctx.reg3 &= ~(1<<6);
  ctx.int1_cfg = 0;
}

void LIS2DH12_DRDY_int_enable()
{
  ctx.reg3 |= (1<<4);
}

void LIS2DH12_DRDY_int_disable()
{
  ctx.reg3 &= ~(1<<4);
}

void LIS2DH12_ODR_set(uint8_t val)
{
  ctx.reg1 = (ctx.reg1 & 0x0F) | val;
}