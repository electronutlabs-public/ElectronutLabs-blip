Vibration Analysis with blip
============================

![microwave_data](microwave_data.png)

## Overview

The objective of this project is to analyze the vibrations produced by any machine. We will use the blip board which is based on Nordic Semiconductor's nRF52840 chip. Blip has on-board LIS2DH12 accelerometer sensor which will help us to capture the vibrations produced by the any electrical device. The data hence obtained will be used to perform FFT(Fast Fourier Transform) to find the dominant frequencies present vibration data.

The firmware will set the accelerometer in high frequency sampling mode to obtain acceleration data at very small time interval. We are choosing the sampling freqeuncy as 1.344 KHz which is one sample every 0.75 ms. We will log the measured data to a SD card. Blip has SD card module on board which makes the task easier. In the end of this section you will find the video demonstrating vibration analysis of a microwave. We have performed FFT of the obtained data using Python to find the dominant frequencies present in vibration data. 

## Hardware Required

* Blip
* SD card
* Battery pack

## Toolchain

The code present in this repository is written on Zephyr RTOS and the data is 
then visualised in jupyter notebook. 

## Compile and Flash the Firmware

Firmware is developed using the Zephyr rtos. Install zephyr from 
[here](https://docs.zephyrproject.org/latest/getting_started/index.html) and follow the instructions to setup the toolchain and SDK.

After setup follow these command line instructions to build and flash the firmware on to Blip.

```
$cd Vibration_Analysis/
$mkdir build && cd build
$cmake -GNinja -DBOARD=nrf52840_blip ..
# Compile
$ninja
# Flash compiled code to blip
$ninja flash
```

## Video

Here is the [video](https://www.youtube.com/watch?v=bWvSZIuPaaA) of project in action.

<iframe width="560" height="315" src="https://www.youtube.com/embed/bWvSZIuPaaA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Code Repository

* <i class="fa fa-git fa-1x" style="color: black"></i> [Blip Vibration Analysis](https://gitlab.com/electronutlabs-public/ElectronutLabs-blip/tree/master/code/projects/Vibration_analysis)