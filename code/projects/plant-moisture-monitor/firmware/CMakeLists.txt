cmake_minimum_required(VERSION 3.8.2)
include($ENV{ZEPHYR_BASE}/cmake/app/boilerplate.cmake NO_POLICY_SCOPE)
project(plant-moisture-monitor)

FILE(GLOB yl_69 src/yl_69/*.c)

target_sources(app PRIVATE
  src/main.c
  ${yl_69}
)

include_directories(src/yl_69)
zephyr_library_include_directories($ENV{ZEPHYR_BASE}/samples/bluetooth)