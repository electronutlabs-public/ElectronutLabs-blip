/**
 * @file yl_69.h
 * @author Ajay Meena
 * @brief YL-69 Soil Moisture Sensor Driver
 * 
 * @copyright Copyright (c) 2019 Electronut Labs
 * 
 */

#ifndef _YL_69_H
#define _YL_69_H
 
#ifdef __cplusplus 
extern "C" { 
#endif

#include <adc.h>
#include <string.h>
#include <hal/nrf_saadc.h>

#define ADC_DEVICE_NAME		DT_ADC_0_NAME
#define ADC_RESOLUTION		10
#define ADC_GAIN		ADC_GAIN_1_6
#define ADC_REFERENCE		ADC_REF_INTERNAL
#define ADC_ACQUISITION_TIME	ADC_ACQ_TIME(ADC_ACQ_TIME_MICROSECONDS, 10)
#define ADC_1ST_CHANNEL_ID	0
#define ADC_1ST_CHANNEL_INPUT	NRF_SAADC_INPUT_AIN1

static const struct adc_channel_cfg m_1st_channel_cfg = {
	.gain             = ADC_GAIN,
	.reference        = ADC_REFERENCE,
	.acquisition_time = ADC_ACQUISITION_TIME,
	.channel_id       = ADC_1ST_CHANNEL_ID,
#if defined(CONFIG_ADC_CONFIGURABLE_INPUTS)
	.input_positive   = ADC_1ST_CHANNEL_INPUT,
#endif
};

// Initialize ADC peripheral for YL-69 sensor
struct device *init_adc(void);

// Read soil moisture data
s16_t read_soil_moisture(void);

#ifdef __cplusplus 
}
#endif
 
#endif /* _YL_69_H */