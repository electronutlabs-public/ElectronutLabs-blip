/**
 * @file yl_69.c
 * @author Ajay Meena
 * @brief YL-69 Soil Moisture Sensor Driver
 * 
 * @copyright Copyright (c) 2019 Electronut Labs
 * 
 */

#include "yl_69.h"

struct device *adc_dev;

// Initialize ADC peripheral for YL-69 sensor
struct device *init_adc(void)
{
	int ret;
	adc_dev = device_get_binding(ADC_DEVICE_NAME);

	if(!adc_dev)
	{
		printk("Cannot get ADC device\n");
	}

	ret = adc_channel_setup(adc_dev, &m_1st_channel_cfg);

	if(ret != 0)
	{
		printk("Setting up of the first channel failed with code %d\n", ret);
	}

	return adc_dev;
}

s16_t read_soil_moisture(void)
{
    static s16_t m_sample_value;

    const struct adc_sequence sequence = {
        .channels    = BIT(ADC_1ST_CHANNEL_ID),
        .buffer      = &m_sample_value,
        .buffer_size = sizeof(m_sample_value),
        .resolution  = ADC_RESOLUTION,
    };

    int ret;
    ret = adc_read(adc_dev, &sequence);

	if(ret != 0)
	{
		printk("adc_read() failed with code %d\n", ret);
	}

    return m_sample_value;
}