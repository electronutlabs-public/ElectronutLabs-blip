/**
 * @file main.c
 * @author Ajay Meena (ajay@electronut.in)
 * @brief Plant moisture monitor firmware
 * 
 * @copyright Copyright (c) 2019, Electronut Labs (electronut.in)
 * 
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <gpio.h>

#include <settings/settings.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

#include "yl_69.h"

struct device *dev;
#define LED_PORT LED0_GPIO_CONTROLLER
#define LED	LED0_GPIO_PIN

#define SLEEP_TIME 	60000

u16_t soil_moisture;

/* Soil moisture Service Variables */
static struct bt_uuid_128 soil_moisture_uuid = BT_UUID_INIT_128(
	0xf0, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);

static struct bt_uuid_128 soil_moisture_enc_uuid = BT_UUID_INIT_128(
	0xf1, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);

static struct bt_gatt_ccc_cfg soil_moisture_ccc_cfg[BT_GATT_CCC_MAX] = {};
static u8_t soil_moisture_notification_enabled;

static void soil_moisture_ccc_cfg_changed(const struct bt_gatt_attr *attr, u16_t value)
{
	soil_moisture_notification_enabled = value == BT_GATT_CCC_NOTIFY;
}

static struct bt_gatt_attr soil_moisture_attrs[] = {
	BT_GATT_PRIMARY_SERVICE(&soil_moisture_uuid),
	BT_GATT_CHARACTERISTIC(&soil_moisture_enc_uuid.uuid,
			       BT_GATT_CHRC_NOTIFY, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE,
			       NULL, NULL, NULL),
	BT_GATT_CCC(soil_moisture_ccc_cfg, soil_moisture_ccc_cfg_changed),
};

static struct bt_gatt_service soil_moisture_svc = BT_GATT_SERVICE(soil_moisture_attrs);

void soil_moisture_notification_complete_cb(struct bt_conn *conn)
{
	// printk("notification complete\n");
}

static void soil_moisture_notify(void)
{
	soil_moisture = read_soil_moisture();
	printk("Moisture data: %d\n", soil_moisture);

	bt_gatt_notify_cb(NULL, &soil_moisture_attrs[1], &soil_moisture, sizeof(soil_moisture), soil_moisture_notification_complete_cb);
	//bt_gatt_notify(NULL, &soil_moisture_attrs[1], &number, sizeof(number));
}

static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR))
};

static void connected(struct bt_conn *conn, u8_t err)
{
	if (err) {
		printk("Connection failed (err %u)\n", err);
	} else {
		printk("Connected\n");
		gpio_pin_write(dev, LED, 0);
	}
}

static void disconnected(struct bt_conn *conn, u8_t reason)
{
	printk("Disconnected (reason %u)\n", reason);
	gpio_pin_write(dev, LED, 1);
}

static struct bt_conn_cb conn_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
};

static void bt_ready(int err)
{
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	printk("Bluetooth initialized\n");

	bt_gatt_service_register(&soil_moisture_svc);

	if (IS_ENABLED(CONFIG_SETTINGS)) {
		settings_load();
	}

	err = bt_le_adv_start(BT_LE_ADV_CONN_NAME, ad, ARRAY_SIZE(ad), NULL, 0);
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}

	printk("Advertising successfully started\n");
}

void main(void)
{
	int err;
	// Enable BLE
	err = bt_enable(bt_ready);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	bt_conn_cb_register(&conn_callbacks);

	dev = device_get_binding(LED_PORT);
	/* Set LED pin as output */
	gpio_pin_configure(dev, LED, GPIO_DIR_OUT);
	gpio_pin_write(dev, LED, 1);

	// Initialize ADC
	struct device *adc_dev = init_adc();
	if (!adc_dev) {
		printk("Failed to initialize ADC");
	}

	while (1) {
		if (soil_moisture_notification_enabled) {
			soil_moisture_notify();
		}

		k_sleep(SLEEP_TIME);
	}
}
