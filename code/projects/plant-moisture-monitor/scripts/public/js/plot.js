var socket = io();
socket.on('moistureData', function (moistureData) {
	window.moistureData = moistureData;
	console.log("moistureData : " + moistureData + ", time: " + (new Date()));
});

Highcharts.theme = {
	global: {
		useUTC: false
	},
	colors: ["#00BCD4"],
	plotOptions: {
		series: {
			dataLabels: {
				color: '#00BCD4'
			},
			turboThreshold : 4321
		}
	}
};

// Apply the theme
Highcharts.setOptions(Highcharts.theme);

var draw_on = document.getElementById('SoilMoistureStream');

$.getJSON('/moisture-data', function (moisture_data) {
	console.log(moisture_data)

	in_min = 1100;
	in_max = 200;
	out_min = 0;
	out_max = 100;

	var data = [];
	// filling the chart
	if(moisture_data.length < 4320)
	{
		var time = new Date(moisture_data[0].time).getTime();

		for (var i = -(4320-moisture_data.length); i <= 0; i += 1) {
			data.push({
				x: time + i * 60000,
				y: 0
			});
		}
	}

	for(var i = 0; i<moisture_data.length; i += 1)
	{
		data.push({
			x: new Date(moisture_data[i].time).getTime(),
			y: (moisture_data[i].value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
		});
	}

	window.chart = new Highcharts.Chart({
		chart: {
			zoomType: 'x',
			renderTo: draw_on,
			backgroundColor: 'rgba(0,0,0,0)',
			events: {
				load: function () {
	
					// set up the updating of the chart each minute
					var series = this.series[0];
					setInterval(function () {
						var x = (new Date()).getTime(), // current time
							y = (window.moistureData - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
						series.addPoint([x, y], true, true);
					}, 60000);
				}
			}
		},
		title: {
			text: 'Soil Moisture Data Stream'
		},
		xAxis: {
			type: 'datetime',
			// labels: {0
			// 	format: '{value:%H-%M-%S}',
			// 	rotation: 45,
			// 	align: 'left'
			// },
			// range: 24 * 3600 * 1000 // one day
		},
		rangeSelector: {
			enabled: true
		},
		yAxis: {
			title: {
				text: 'Value'
			},
			min: 0,
			max: 100,
			tickInterval: 10
		},
		legend: {
			enabled: false
		},
		plotOptions: {
			area: {
				fillColor: {
					linearGradient: {
						x1: 0,
						y1: 0,
						x2: 0,
						y2: 1
					},
					stops: [
						[0, Highcharts.getOptions().colors[0]],
						[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
					]
				},
				marker: {
					radius: 0.5
				},
				lineWidth: 1,
				states: {
					hover: {
						lineWidth: 1
					}
				},
				threshold: null
			}
		},
	
		series: [{
			type: 'area',
			name: 'Soil Moisture Data Stream',
			data: data
		}]
	});

});