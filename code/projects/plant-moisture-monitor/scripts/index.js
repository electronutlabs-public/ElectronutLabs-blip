var noble = require('noble');
const path = require('path');
const express = require('express')
const app = express()
var http = require('http').Server(app);
var io = require('socket.io')(http);
const port = 3000

require('dotenv').config()

const mongoose = require('mongoose');
mongoose.connect(process.env.DB_CONN, {
    useNewUrlParser: true
});

// Define schema
var Schema = mongoose.Schema;

var moistureDataSchema = new Schema({
    value: Number,
    time: String
});

const dataPoints = mongoose.model('dataPoints', moistureDataSchema);

// Moisture sensor service and characteristic
var serviceUUIDs = ['1234567812345678123456789abcdef0'];
var characteristicUUIDs = ['1234567812345678123456789abcdef1'];

// Start scanning ble devices
noble.on('stateChange', function (state) {
    console.log(state);
    if (state === 'poweredOn') {
        console.log('scanning...');
        noble.startScanning([], false);
    } else {
        console.log('stopped.');
        noble.stopScanning();
    }
});

noble.on('scanStart', function () {
    console.log('scan started');
});

// this event is emitted when any ble device is found.
noble.on('discover', function (peripheral) {
    if (peripheral.advertisement.localName) {
        console.log(peripheral.advertisement.localName);

        // check for moisture sensor device
        if (peripheral.advertisement.localName.replace(/\0/g, '') == 'soil-moisture') {
            console.log("device found");

            noble.stopScanning();

            //connect to the device
            peripheral.connect(function (error) {
                if (error) throw error;
                else {
                    console.log("connected")

                    // discover moisture sensor service and characteristic
                    peripheral.discoverSomeServicesAndCharacteristics(serviceUUIDs, characteristicUUIDs, function (error, services, characteristics) {
                        if (error) throw error;
                        else {
                            for (i = 0; i < characteristics.length; i++) {
                                if (characteristics[i].uuid == characteristicUUIDs[0]) {
                                    characteristic = characteristics[i];
                                    console.log('characteristic uuid found : ', characteristic.uuid);

                                    // subscribe to moisture data notifications
                                    characteristic.subscribe(function (error) {
                                        if (error) throw error;
                                    });

                                    characteristic.on('data', function (data, isNotification) {
                                        console.log(data.readInt16LE(0));
                                        moistureData = data.readInt16LE(0);
                                        io.emit('moistureData', moistureData);

                                        const currentDataPoint = new dataPoints({
                                            value: moistureData,
                                            time: new Date()
                                        });

                                        currentDataPoint.save().then(() => console.log('saved'));
                                    });
                                }
                            }
                        }
                    });
                }
            })
        }
    }
});

io.on('connection', function (socket) {
    console.log('a user connected');
});

// get moisture data for last 3 days
app.get('/moisture-data', function (req, res) {
    dataPoints.find({}).sort({ _id: -1 }).limit(4320).exec(function (err, data_points) {
        if(err) console.log(err);
        res.json(JSON.parse(JSON.stringify(data_points.reverse())))
    });
})

app.use(express.static(path.join(__dirname, 'public')))
http.listen(port, () => console.log(`app listening on port ${port}!`))