![wall_photo](wall_photo.png)

## Overview
Blip firmware reads the analog data of moisture sensor and writes to a ble service characteristic periodically every minute. A node.js [script](scripts/index.js) is running on a raspberry pi which connects to blip and subscribes to moisture service notification. When it gets the moisture data, it appends that to the time series graph on Web UI and write to MongoDB database as well.

## Hardware components Used
1. [Blip](https://docs.electronut.in/blip/)
2. Analog Soil Moisture Sensor 
3. Raspberry Pi 3 B+

## Dependencies
### Firmware
1. Zephyr OS

### Software and visualization
1. Node.js, Express.js
2. socket.io, mongoose
3. [noble](https://github.com/noble/noble)
4. Highcharts

## Usage
To make use of this project, you should have a [Blip board](https://docs.electronut.in/blip/) and a generic soil moisture sensor, connections should be as follows:
#### Connections
Blip | Soil Moisture sensor
--- | ---
+5V | VCC
GND | GND
P0.03 | VO

V0 is amplified analog output of conductivity between electrodes which changes with the moisture level.

#### Compiling and Flashing the Firmware
Firmware is developed using the Zephyr SDK, Please install zephyr from [here](https://docs.zephyrproject.org/latest/getting_started/index.html) and then execute following commands.
```
cd firmware
mkdir build && cd build
cmake -GNinja -DBOARD=nrf52840_blip ..
# Compile
ninja
# Flash
ninja flash
```
#### Running Node.js script and visualization
Node.js scripts extract the soil moisture data from ble packets and visualize the feeds on web browser.
Please install [Node.js](https://nodejs.org/en/) and run these commands.
```
cd scripts
sudo node index.js
```
![moisture feed](images/feed.png)

## Demo Video
Here is the [video](https://www.youtube.com/watch?v=jvb3YalYmyw) of project in action.

<iframe width="560" height="315" src="https://www.youtube.com/embed/jvb3YalYmyw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Source Code
Firmware, node.js script and visualization code is available [here](https://gitlab.com/electronutlabs-public/ElectronutLabs-blip/tree/master/code/projects/plant-moisture-monitor).