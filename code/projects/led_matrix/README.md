## Overview
This project is about interfacing 8x8 led matrix with blip board, transfer and display led patterns on matrix over bluetooth low energy. Arduino IDE is used for this project.
You can talk directly to Blip using [Web Bluetooth](https://webbluetoothcg.github.io/web-bluetooth/). For this to work, your browser must support Web Bluetooth and the blip board should have the necessary firmware, please check this [blog post](https://electronut.in/led-matrix-controlled-via-web-bluetooth-on-blip/) for more detail. To check list of available browsers, visit this [link](https://developer.mozilla.org/en-US/docs/Web/API/Web_Bluetooth_API#Browser_compatibility). On Google Chrome, you should have enabled experimental web features by visiting the url :
```chrome://flags/#enable-experimental-web-platform-features```

## Hardware components used
1. Blip board
2. 8×8 RGB Led Matrix (CILE 2088RGB-8)
3. Shift registers (74HC595)

## Connections
Four cascaded configuration of shift register is used for controlling 32 pins of led matrix.

| Blip | Cascaded Shift registers |
| --- | --- |
| P0.03 | DATA |
| P0.04 | OUTPUT ENABLE |
| P0.05 | LATCH (RCLK) |
| P0.06 | CLOCK (SRCLK) |
| P0.07 | RESET |

For more detail on schematic and connections, check out this [blog post](https://electronut.in/led-matrix-controlled-via-web-bluetooth-on-blip/).

## Try it here
To Use this, First flash the [arduino firmware](https://gitlab.com/electronutlabs-public/ElectronutLabs-blip/tree/master/code/projects/led_matrix/LED8x8_Matrix_BLE) and make sure that led matrix is working properly.
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
<script src="./web-bluetooth/led-matrix.js"></script>
<style>
td{
    padding: 0px !important;
}
.circle{
    width: 35px;
    height: 35px;
    margin: 4px;
    background: gray;
    border-radius: 50%;
}
.box{
    background: black;
}
.box:hover{
    background: black !important;
}
.color{
    width: 40px;
    height: 40px;
    background: black;
    margin: 3px;
    border:1px solid gray;
}
.lift{
    box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);;
}
.green{
    background : green;
}
.red{
    background : red;
}
.yellow{
    background : yellow;
}
.blue{
    background : blue;
}
.cyan{
    background : cyan;
}
.magenta{
    background : magenta;
}
.white{
    background : white;
}
.border{
    border-right: 1px solid gray;
    border-bottom: 1px solid gray;
}
</style>
<div  ng-app="blipApp" ng-controller="ledMatrixController" ng-mouseup="mouseUp()">

<div style="margin: 0 auto; text-align: -webkit-center;">
<button data-md-color-primary="light-blue" id="connect">Connect</button>
<button data-md-color-primary="light-blue" id="send">Send</button>
<button data-md-color-primary="light-blue" id="clear">Clear</button>
</div>
<div style="margin: 0 auto; text-align: -webkit-center;">
    <div style="margin: 0 auto;     text-align: -webkit-center;">
        <table>
            <tbody>
                <tr class="box" style="display: flex;" ng-repeat="items in arr" ng-init="parentIndex = $index">
                    <td class="border" ng-repeat="item in items"
                    ng-init="childIndex = $index">
            <div class="circle" ng-class="item.color" ng-mousedown="mouseDown(parentIndex,childIndex)" ng-mouseup="mouseUp()"
            ng-mouseover="mouseOver(parentIndex,childIndex)" ng-click="fillColor(parentIndex,childIndex)">
            </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin: 20 auto;     text-align: -webkit-center;">
        <table>
            <tbody>
                <tr style="display: flex;">
                    <td class="color off" ng-class="selectedColor == 'off'? 'lift' : ''" ng-click="selectColor('off')"></td>
                    <td class="color green" ng-class="selectedColor == 'green'? 'lift' : ''"  ng-click="selectColor('green')"></td>
                    <td class="color red" ng-class="selectedColor == 'red'? 'lift' : ''"  ng-click="selectColor('red')"></td>
                    <td class="color yellow" ng-class="selectedColor == 'yellow'? 'lift' : ''"  ng-click="selectColor('yellow')"></td>
                    <td class="color blue" ng-class="selectedColor == 'blue'? 'lift' : ''"  ng-click="selectColor('blue')"></td>
                    <td class="color cyan" ng-class="selectedColor == 'cyan'? 'lift' : ''"  ng-click="selectColor('cyan')"></td>
                    <td class="color magenta" ng-class="selectedColor == 'magenta'? 'lift' : ''"  ng-click="selectColor('magenta')"></td>
                    <td class="color white" ng-class="selectedColor == 'white'? 'lift' : ''"  ng-click="selectColor('white')"></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
</div>

## Demo Video
Here is the [video](https://www.youtube.com/watch?v=T1Pt7izVmaw) of project in action.

<iframe width="560" height="315" src="https://www.youtube.com/embed/T1Pt7izVmaw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Source Code
[LED Matrix BLE Repo](https://gitlab.com/electronutlabs-public/ElectronutLabs-blip/tree/master/code/projects/led_matrix)