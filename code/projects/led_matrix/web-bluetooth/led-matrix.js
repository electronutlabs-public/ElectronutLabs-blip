angular.module('blipApp', [])
    .controller('ledMatrixController', function ($scope) {
        var Writecharacteristic;
        var connect = document.getElementById("connect");
        var send = document.getElementById("send");
        var clear = document.getElementById("clear");
        send.disabled = true;
        send.setAttribute("data-md-color-primary", "grey");

        connect.addEventListener('click', function (event) {
            if (connect.innerText == "Connect") {
                navigator.bluetooth.requestDevice({
                    acceptAllDevices: true,
                    optionalServices: ['6e400001-b5a3-f393-e0a9-e50e24dcca9e']
                })
                .then(device => {
                    bluetoothDevice = device;
                    bluetoothDevice.addEventListener('gattserverdisconnected', onDisconnected);
                    // Human-readable name of the device.
                    console.log('Connecting to GATT Server...' + device.name);
                    // Attempts to connect to remote GATT Server.
                    return device.gatt.connect();
                })
                .then(server => {
                    // Note that we could also get all services that match a specific UUID by
                    // passing it to getPrimaryServices().
                    console.log('Getting Services...');
                    return server.getPrimaryService('6e400001-b5a3-f393-e0a9-e50e24dcca9e');
                })
                .then(service => {
                    service.getCharacteristics().then(characteristics => {
                        console.log(characteristics);
                        send.disabled = false;
                        connect.innerText = "Disconnect";
                        send.setAttribute("data-md-color-primary", "light-blue");
                        console.log('> Service: ' + service.uuid);
                        console.log(characteristics[0].uuid)

                        var TXcharacteristic = characteristics[0];
                        Writecharacteristic = characteristics[1];
                        TXcharacteristic.startNotifications().then(_ => {
                            console.log('> Notifications started');
                            console.log(characteristics[1].uuid)

                        });
                    })
                })
                .catch(error => {
                    console.log('Argh! ' + error);
                });
            } else {
                if (!bluetoothDevice) {
                    return;
                }
                console.log('Disconnecting from Bluetooth Device...');
                if (bluetoothDevice.gatt.connected) {
                    bluetoothDevice.gatt.disconnect();
                } else {
                    console.log('> Bluetooth Device is already disconnected');
                }
            }
                
        });

        function onDisconnected(event) {
            // Object event.target is Bluetooth Device getting disconnected.
            send.disabled = true;
            send.setAttribute("data-md-color-primary", "grey");
            console.log('> Bluetooth Device disconnected');
            connect.innerText = "Connect"
        }

        send.addEventListener('click', function (event) {
            var LedMatrix = Create2DArray(8, 8);
            console.log(LedMatrix);
            if (Writecharacteristic) {
                Writecharacteristic.writeValue(str2ab('8' + ArrayToString(LedMatrix) + '\n'));
            } else {
                alert("Device not connected")
            }
        });

        clear.addEventListener('click', async function (event) {
            console.log($scope.allClear);
            
            $scope.arr = await angular.copy($scope.allClear);
            $scope.$apply();
        });


        function str2ab(str) {
            var buf = new ArrayBuffer(str.length);
            var bufView = new Uint8Array(buf);
            for (var i = 0, strLen = str.length; i < strLen; i++) {
                bufView[i] = str.charCodeAt(i);
            }
            return buf;
        }


        function ArrayToString(arr) {
            var str = "";

            for (var i = 0; i < arr.length; i++) {
                for (var j = 0; j < arr[i].length; j++) {
                    str += arr[i][j].toString();
                }
            }
            return str;
        }


        var map = {
            off: 0,
            green: 1,
            red: 2,
            yellow: 3,
            blue: 4,
            cyan: 5,
            magenta: 6,
            white: 7,
        }

        function Create2DArray(columns, rows) {
            var arr = [];
            for (var i = 0; i < rows; i++) {
                if (!arr[i]) {
                    arr[i] = [
                        map[$scope.arr[i][7].color],
                        map[$scope.arr[i][6].color],
                        map[$scope.arr[i][5].color],
                        map[$scope.arr[i][4].color],
                        map[$scope.arr[i][3].color],
                        map[$scope.arr[i][2].color],
                        map[$scope.arr[i][1].color],
                        map[$scope.arr[i][0].color],
                    ];
                }
            }

            return arr;
        }


        async function init(){
            $scope.widthArr = [1, 2, 3, 4, 5, 6, 7, 8];
            $scope.heightArr = [1, 2, 3, 4, 5, 6, 7, 8];
            $scope.arr = [];
            for (var i = 0; i < 8; i++) {
                if (!$scope.arr[i]) {
                    $scope.arr[i] = [];
                }
                for (var j = 0; j < 8; j++) {
                    if (!$scope.arr[i][j]) {
                        $scope.arr[i][j] = {
                            color: "off"
                        };
                        await console.log();
                    }
                }
            }
            $scope.$apply();
            setTimeout(() => {
                $scope.allClear = angular.copy($scope.arr);
            }, 1000);
        }

        init();


        $scope.fillColor = function (i, j) {
            // $scope.selectedColor = color;
            $scope.arr[i][j].color = $scope.selectedColor;

        }
        $scope.selectColor = function (color) {
            $scope.selectedColor = color;
        }

        $scope.mouseDown = function (i, j) {
            $scope.fillColor(i, j)
            $scope.mouseOver = function (parentIndex, childIndex) {
                $scope.fillColor(parentIndex, childIndex)
            }
        }
        $scope.mouseUp = function () {
            $scope.mouseOver = function (parentIndex, childIndex) {
            }

        }


    });