/** @file ili9163_spi.c
 *
 * @brief ILI9163 SPI driver
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Tavish Naruka <tavish@electronut.in>
 * All rights reserved. 
*/

#include "ili9163_spi.h"
#include "lcdif.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(ili9163);

#define __bswap_16(x) ((u16_t) ((((x) >> 8) & 0xff) | (((x) & 0xff) << 8)))

#define CMD_NOP					0x01
#define CMD_SOFT_RESET				0x01
#define CMD_GET_RED_CHANNEL			0x06
#define CMD_GET_GREEN_CHANNEL			0x07
#define CMD_GET_BLUE_CHANNEL			0x08
#define CMD_GET_PIXEL_FORMAT			0x0C
#define CMD_GET_POWER_MODE			0x0A
#define CMD_GET_ADDRESS_MODE			0x0B
#define CMD_GET_DISPLAY_MODE			0x0D
#define CMD_GET_SIGNAL_MODE			0x0E
#define CMD_GET_DIAGNOSTIC_RESULT		0x0F
#define CMD_ENTER_SLEEP_MODE			0x10
#define CMD_EXIT_SLEEP_MODE			0x11
#define CMD_ENTER_PARTIAL_MODE			0x12
#define CMD_ENTER_NORMAL_MODE			0x13
#define CMD_EXIT_INVERT_MODE			0x20
#define CMD_ENTER_INVERT_MODE			0x21
#define CMD_SET_GAMMA_CURVE			0x26
#define CMD_SET_DISPLAY_OFF			0x28
#define CMD_SET_DISPLAY_ON			0x29
#define CMD_SET_COLUMN_ADDRESS			0x2A
#define CMD_SET_PAGE_ADDRESS			0x2B
#define CMD_WRITE_MEMORY_START			0x2C
#define CMD_WRITE_LUT				0x2D
#define CMD_READ_MEMORY_START			0x2E
#define CMD_SET_PARTIAL_AREA			0x30
#define CMD_SET_SCROLL_AREA			0x33
#define CMD_SET_TEAR_OFF			0x34
#define CMD_SET_TEAR_ON				0x35
#define CMD_SET_MEMORY_ACCESS_CTRL		0x36
#define CMD_SET_SCROLL_START			0x37
#define CMD_EXIT_IDLE_MODE			0x38
#define CMD_ENTER_IDLE_MODE			0x39
#define CMD_SET_PIXEL_FORMAT			0x3A
#define CMD_WRITE_MEMORY_CONTINUE		0x3C
#define CMD_READ_MEMORY_CONTINUE		0x3E
#define CMD_SET_TEAR_SCANLINE			0x44
#define CMD_GET_SCANLINE			0x45
#define CMD_READ_ID1				0xDA
#define CMD_READ_ID2				0xDB
#define CMD_READ_ID3				0xDC
#define CMD_FRAME_RATE_CONTROL1			0xB1
#define CMD_FRAME_RATE_CONTROL2			0xB2
#define CMD_FRAME_RATE_CONTROL3			0xB3
#define CMD_DISPLAY_INVERSION			0xB4
#define CMD_SOURCE_DRIVER_DIRECTION		0xB7
#define CMD_GATE_DRIVER_DIRECTION		0xB8
#define CMD_POWER_CONTROL1			0xC0
#define CMD_POWER_CONTROL2			0xC1
#define CMD_POWER_CONTROL3			0xC2
#define CMD_POWER_CONTROL4			0xC3
#define CMD_POWER_CONTROL5			0xC4
#define CMD_VCOM_CONTROL1			0xC5
#define CMD_VCOM_CONTROL2			0xC6
#define CMD_VCOM_OFFSET_CONTROL			0xC7
#define CMD_WRITE_ID4_VALUE			0xD3
#define CMD_NV_MEMORY_FUNCTION1			0xD7
#define CMD_NV_MEMORY_FUNCTION2			0xDE
#define CMD_POSITIVE_GAMMA_CORRECT		0xE0
#define CMD_NEGATIVE_GAMMA_CORRECT		0xE1
#define CMD_GAM_R_SEL				0xF2

static void write_command(u8_t c)
{
	DC_LOW();
	CS_LOW();
	lcdif_spi_send(&c, 1);
	CS_HIGH();
}

static void write_data8(u8_t c)
{
	DC_HIGH();
	CS_LOW();
	lcdif_spi_send(&c, 1);
	CS_HIGH();
}

static void write_data16(u16_t d)
{
	u8_t c = d >> 8;
	DC_HIGH();
	CS_LOW();
	lcdif_spi_send(&c, 1);
	c = d & 0xff;
	lcdif_spi_send(&c, 1);
	CS_HIGH();
}


// see  https://github.com/sumotoy/TFT_ILI9163C/blob/master/TFT_ILI9163C.cpp
// see https://github.com/radhoo/ILI9163_LCD/blob/master/spi/ili9163/ili9163lcd.cpp

// pixels are stored row first, each row is ILI9163_WIDTH wide
static uint16_t frame_buffer[ILI9163_HEIGHT * ILI9163_WIDTH];

void ili9163_init()
{
	lcdif_init();

	CS_HIGH();
	RESET_HIGH();

	// reset the LCD
	ili9163_reset();

	write_command(CMD_EXIT_SLEEP_MODE);
	delay_ms(5);

	write_command(CMD_SET_PIXEL_FORMAT);
	write_data8(0x05); // 16bpp

	write_command(CMD_SET_GAMMA_CURVE);
	write_data8(0x04); // Select gamma curve 3

	write_command(CMD_GAM_R_SEL);
	write_data8(0x01); // Gamma adjustment enabled

	write_command(CMD_POSITIVE_GAMMA_CORRECT);
	write_data8(0x3f); // 1st Parameter
	write_data8(0x25); // 2nd Parameter
	write_data8(0x1c); // 3rd Parameter
	write_data8(0x1e); // 4th Parameter
	write_data8(0x20); // 5th Parameter
	write_data8(0x12); // 6th Parameter
	write_data8(0x2a); // 7th Parameter
	write_data8(0x90); // 8th Parameter
	write_data8(0x24); // 9th Parameter
	write_data8(0x11); // 10th Parameter
	write_data8(0x00); // 11th Parameter
	write_data8(0x00); // 12th Parameter
	write_data8(0x00); // 13th Parameter
	write_data8(0x00); // 14th Parameter
	write_data8(0x00); // 15th Parameter

	write_command(CMD_NEGATIVE_GAMMA_CORRECT);
	write_data8(0x20); // 1st Parameter
	write_data8(0x20); // 2nd Parameter
	write_data8(0x20); // 3rd Parameter
	write_data8(0x20); // 4th Parameter
	write_data8(0x05); // 5th Parameter
	write_data8(0x00); // 6th Parameter
	write_data8(0x15); // 7th Parameter
	write_data8(0xa7); // 8th Parameter
	write_data8(0x3d); // 9th Parameter
	write_data8(0x18); // 10th Parameter
	write_data8(0x25); // 11th Parameter
	write_data8(0x2a); // 12th Parameter
	write_data8(0x2b); // 13th Parameter
	write_data8(0x2b); // 14th Parameter
	write_data8(0x3a); // 15th Parameter

	write_command(CMD_FRAME_RATE_CONTROL1);
	write_data8(0x08); // DIVA = 8
	write_data8(0x08); // VPA = 8

	write_command(CMD_DISPLAY_INVERSION);
	write_data8(0x07); // NLA = 1, NLB = 1, NLC = 1 (all on Frame Inversion)

	write_command(CMD_POWER_CONTROL1);
	write_data8(0x0a); // VRH = 10:  GVDD = 4.30
	write_data8(0x02); // VC = 2: VCI1 = 2.65

	write_command(CMD_POWER_CONTROL2);
	write_data8(0x02); // BT = 2: AVDD = 2xVCI1, VCL = -1xVCI1, VGH = 5xVCI1, VGL = -2xVCI1

	write_command(CMD_VCOM_CONTROL1);
	write_data8(0x50); // VMH = 80: VCOMH voltage = 4.5
	write_data8(0x5b); // VML = 91: VCOML voltage = -0.225

	write_command(CMD_VCOM_OFFSET_CONTROL);
	write_data8(0x40); // nVM = 0, VMF = 64: VCOMH output = VMH, VCOML output = VML

	write_command(CMD_SET_COLUMN_ADDRESS);
	write_data8(0x00); // XSH
	write_data8(0x00); // XSL
	write_data8(0x00); // XEH
	write_data8(0x7f); // XEL (128 pixels x)

	write_command(CMD_SET_PAGE_ADDRESS);
	write_data8(0x00);
	write_data8(0x00);
	write_data8(0x00);
	write_data8(0x7f); // 128 pixels y

	// TODO set rotation
	write_command(CMD_SET_MEMORY_ACCESS_CTRL);
	write_data8(0x20 | 0x08);

	// Set the display to on
	write_command(CMD_SET_DISPLAY_ON);
	write_command(CMD_WRITE_MEMORY_START);
}

void ili9163_reset(void)
{
	RESET_LOW();
	delay_ms(50);
	RESET_HIGH();
	delay_ms(120);
}

void ili9163_set_address(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	write_command(CMD_SET_COLUMN_ADDRESS);
	write_data16(x1);
	write_data16(x2);

	write_command(CMD_SET_PAGE_ADDRESS);
	write_data16(y1);
	write_data16(y2);
	// memory write
	write_command(CMD_WRITE_MEMORY_START);
}

void ili9163_set_rotation(rotation_t rotation)
{
	// TODO, we need to support runtime orientation change
}

struct spi_buf row_bufs[160];

void ili9163_draw_rect_filled(uint16_t x, uint16_t y, uint16_t w, uint16_t h,
		uint16_t colour)
{
	u16_t copy_x = x;
	u16_t copy_y = y;

	// byte order needs swapping
	colour = __bswap_16(colour);

	if ((x >= ILI9163_WIDTH) || (y >= ILI9163_HEIGHT))
		return;
	if ((x + w - 1) >= ILI9163_WIDTH) {
		w = ILI9163_WIDTH - x;
	}
	if ((y + h - 1) >= ILI9163_HEIGHT) {
		h = ILI9163_HEIGHT - y;
	}
	

	for(y = copy_y; y < (copy_y + h); y++) {
		for(x = copy_x; x < (copy_x + w); x++) {
			frame_buffer[y*ILI9163_WIDTH + x] = colour;
		}
	}

	// ili9163_set_address(x, y, x + w - 1, y + h - 1);
	// DC_HIGH();
	// CS_LOW();

	// int i;
	// spi_multi_buf_t spi_bufs;
	// for (i=0, y = copy_y; y < (copy_y + h); y++, i++) {
	// 	// lcdif_spi_send((u8_t *)&frame_buffer[y*ILI9163_WIDTH + copy_x], w * 2);
	// 	row_bufs[i].buf = (u8_t *)&frame_buffer[y*ILI9163_WIDTH + copy_x];
	// 	row_bufs[i].len = w*2;
	// }
	// spi_bufs.bufs = row_bufs;
	// spi_bufs.num_bufs = h;
	// lcdif_spi_send_multi(&spi_bufs);

	// CS_HIGH();
}

void ili9163_draw_frame()
{
	ili9163_set_address(0, 0, ILI9163_WIDTH, ILI9163_HEIGHT);
	DC_HIGH();
	CS_LOW();
	lcdif_spi_send((u8_t *)frame_buffer, sizeof(frame_buffer));
	CS_HIGH();
}

void ili9163_draw_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h,
		uint16_t colour)
{
	ili9163_draw_rect_filled(x, y, w, 1, colour);
	ili9163_draw_rect_filled(x+w-1, y, 1, h, colour);
	ili9163_draw_rect_filled(x, y+h-1, w, 1, colour);
	ili9163_draw_rect_filled(x, y, 1, h, colour);
}