/** @file lcdif.h
 *
 * @brief board/port specific code for an SPI LCD
 *
 * @par
* COPYRIGHT NOTICE: (c) 2019 Tavish Naruka <tavish@electronut.in>
 * All rights reserved. 
*/

#ifndef __PINS_BYPASS_H__
#define __PINS_BYPASS_H__

#include <zephyr.h>
#include <device.h>
#include <spi.h>

/*
we're going to skip Zephyr's GPIO API, and use nrfx HAL API, probably doesn't
make that much difference in speec since eeven for long SPI transfers it is 
used only 4 times.
*/

#include <hal/nrf_gpio.h>

// TODO cs pin not connected on your board
// TODO check other pins
#define CS_INIT()	NRF_P0->DIRSET = (1UL << 4);
#define CS_HIGH()	nrf_gpio_port_out_set(NRF_P0, BIT(4))
#define CS_LOW()	nrf_gpio_port_out_clear(NRF_P0, BIT(4))

#define DC_INIT()	NRF_P1->DIRSET = (1UL << 1);
#define DC_HIGH()	nrf_gpio_port_out_set(NRF_P1, BIT(1))
#define DC_LOW()	nrf_gpio_port_out_clear(NRF_P1, BIT(1))

#define RESET_INIT()	NRF_P1->DIRSET = (1UL << 0);
#define RESET_HIGH()	nrf_gpio_port_out_set(NRF_P1, BIT(0))
#define RESET_LOW()	nrf_gpio_port_out_clear(NRF_P1, BIT(0))

#define BYPASS_PINS_INIT()	do{CS_INIT(); DC_INIT(); RESET_INIT();}while(0)

static inline void delay_ms(int ms)
{
	k_sleep(ms);
}

typedef struct {
	struct spi_buf *bufs;
	int num_bufs;
}spi_multi_buf_t;

void lcdif_init();
void lcdif_spi_send(u8_t *buf, int len);
void lcdif_spi_send_multi(spi_multi_buf_t *);

#endif /* __PINS_BYPASS_H__ */