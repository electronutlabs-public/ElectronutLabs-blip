/** @file ili9163_spi.h
 *
 * @brief ILI9163 SPI driver header
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Tavish Naruka <tavish@electronut.in>
 * All rights reserved. 
*/

#ifndef __ILI9163_H__
#define __ILI9163_H__

#include <stdint.h>

#define ILI9163_HEIGHT				128
#define ILI9163_WIDTH				160

typedef enum {
	ROTATION_0 = 0,
	ROTATION_90 = 0x60,
	ROTATION_180 = 0,
	ROTATION_270 = 0,
}rotation_t;

void ili9163_init();
void ili9163_reset(void);
void ili9163_draw_rect_filled(uint16_t x, uint16_t y, uint16_t w, uint16_t h,
	uint16_t colour);
void ili9163_draw_rect(uint16_t x, uint16_t y, uint16_t w, uint16_t h,
	uint16_t colour);
void ili9163_draw_frame();

#endif // __ILI9163_H__
