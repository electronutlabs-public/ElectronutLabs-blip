/** @file lcdif.h
 *
 * @brief board/port specific code for an SPI LCD
 *
 * @par
* COPYRIGHT NOTICE: (c) 2019 Tavish Naruka <tavish@electronut.in>
 * All rights reserved. 
*/


#include "lcdif.h"

static struct spi_config lcdif_spi_cfg;
static struct device *lcdif_spidev;

void lcdif_init()
{
	BYPASS_PINS_INIT();
	lcdif_spi_cfg.operation = SPI_WORD_SET(8);
	lcdif_spi_cfg.frequency = 32000000U;

	lcdif_spidev = device_get_binding(DT_NORDIC_NRF_SPI_SPI_3_LABEL);
}

void lcdif_spi_send(u8_t *buf, int len)
{
	struct spi_buf spi_tx_buf = {
		.buf = buf,
		.len = len
	};
	struct spi_buf_set buf_set = {
		.buffers = &spi_tx_buf,
		.count = 1
	};
	
	spi_write(lcdif_spidev, &lcdif_spi_cfg, &buf_set);
}

void lcdif_spi_send_multi(spi_multi_buf_t *mbuf)
{
	struct spi_buf_set buf_set = {
		.buffers = mbuf->bufs,
		.count = mbuf->num_bufs
	};
	
	spi_write(lcdif_spidev, &lcdif_spi_cfg, &buf_set);
}
