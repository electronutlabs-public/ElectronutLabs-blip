/** @file main.c
 *
 * @brief Sample application which uses TFTM018 LCD
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2019 Electronut Labs.
 * All rights reserved. 
*/

#include <zephyr.h>
#include <device.h>
#include <sensor.h>
#include <spi.h>
#include "arm_math.h"

#include "ili9163_spi.h"
#include "interpolated_colors.h"

#define COLORDEPTH	1024
#define MINTEMP		2600
#define MAXTEMP		3200

#define INPUT_GRID_SIZE	8 //fixed
#define GRID_SIZE	128
#define SCREEN_WIDTH	128

static struct sensor_value temp_value[64];
static q15_t temp_value_q15[64];

static q15_t constrain_q15(q15_t val, q15_t min_val, q15_t max_val) {
	return MIN(max_val, MAX(min_val, val));
}

static uint32_t map_q15(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max) {
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

arm_bilinear_interp_instance_q15 interp_q15 = {
	.numCols = INPUT_GRID_SIZE,
	.numRows = INPUT_GRID_SIZE,
	.pData = temp_value_q15,
};

void print_buffer(void *ptr, size_t l)
{
	struct sensor_value *tv = ptr;

	for(int i=0; i<64; i++) {
		temp_value_q15[i] = (tv[i].val1*100 + tv[i].val2 / 10000);
		temp_value_q15[i] = constrain_q15(temp_value_q15[i], MINTEMP, MAXTEMP);
	}

	// There is a bug here somewhere, 1 row and 1 pixel are wrong
	// so probably not using the interpolation function correctly.
	u8_t tile_w = SCREEN_WIDTH/GRID_SIZE;
	u8_t div = GRID_SIZE/INPUT_GRID_SIZE;
	for(uint32_t x=0; x<GRID_SIZE; x++) {
		for(uint32_t y=0; y<GRID_SIZE; y++) {
			q15_t interp_val = arm_bilinear_interp_q15(
				&interp_q15, 
				((x/div)<<20) | (x%div)*(((1000/div)<<20)/1000),
				((y/div)<<20) | (y%div)*(((1000/div)<<20)/1000));
			
			interp_val = constrain_q15(interp_val, MINTEMP, MAXTEMP);

			uint32_t color_val;
			color_val = map_q15(interp_val, 
				MINTEMP, MAXTEMP, 0, 
				COLORDEPTH - 1);

			ili9163_draw_rect_filled(x*tile_w, y*tile_w, tile_w, 
				tile_w, hue_line[color_val]);
		}
	}
	
	ili9163_draw_frame();
}

void draw_error_symbol()
{
	ili9163_draw_rect_filled(0, 0, ILI9163_WIDTH, ILI9163_HEIGHT, 0x001F);
	ili9163_draw_rect_filled(15, 15, 
			ILI9163_WIDTH-30, ILI9163_HEIGHT-30, 0xFFFF);
	while(1) {
		ili9163_draw_rect_filled(ILI9163_WIDTH/2 -10, ILI9163_HEIGHT-45,
				20, 20, 0x001F);
		ili9163_draw_rect_filled(ILI9163_WIDTH/2 -10, 25,
				20, ILI9163_HEIGHT-75, 0x001F);
		k_sleep(500);
		ili9163_draw_rect_filled(15, 15,
				ILI9163_WIDTH-30, ILI9163_HEIGHT-30, 0xFFFF);
		k_sleep(500);
	}
}

void main()
{
	struct device *grid_dev = NULL;

	printk("TFTM08 sample application\n");

	ili9163_init();

	ili9163_draw_rect_filled(0, 0,
		ILI9163_WIDTH, ILI9163_HEIGHT, 0x0000);

	grid_dev = device_get_binding(CONFIG_AMG88XX_NAME);

	if(!grid_dev)
	{
		draw_error_symbol();
	}

	while(1) {
		int ret = sensor_sample_fetch(grid_dev);
		if (ret) {
			printk("Failed to fetch a sample, %d\n", ret);
			return;
		}
		
		ret = sensor_channel_get(grid_dev, SENSOR_CHAN_AMBIENT_TEMP,
					 (struct sensor_value *)temp_value);
		if (ret) {
			printk("Failed to get sensor values, %d\n", ret);
			return;
		}

		printk("new sample:\n");
		print_buffer(temp_value, ARRAY_SIZE(temp_value));
	}
}
