Thermal grid sensor example
===========================

![](./amg8833.png)

In this sample, blip shows the data from an 8x8 thermal grid sensor on an
ILI9163 LCD driver based TFT screen.

Required Parts:

* Blip
* [Adafruit AMG8833 module](https://www.adafruit.com/product/3538)
* TFTM018 LCD or similar ILI9163 based LCD (3-wire SPI mode)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/TKabGTawxCs?start=11" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Connections
-----------

[TFTM018](https://www.buydisplay.com/default/display-1-8-inch-spi-128x160-tft-touch-screen-lcd-module-datasheet) LCD module from buy-lcd.com

**TFTM018**|**Name**|**Pin**
:-----:|:-----:|:-----:
3| SCK| P0.29
8| MOSI| P0.30
2| RESET| P1.0
6| D/C| P1.1
4| CS| P0.4
1| VDD| VDD
5| GND| GND

[AMG8833 thermal camera](https://www.adafruit.com/product/3538) module from Adafruit

**AMG8833 pin**|**Blip pin**
:--------------|-----------:
Vin| Vdd
Gnd| Gnd
SDA| SDA/P0.12
SCL| SCL/P0.11

Firmware
========

For this sample we are using [Zephyr](https://zephyrproject.org/), which is an
open source RTOS for embedded devices. Blip is supported in Zephyr, and here is
the documentation page for the board in ther [docs](https://docs.zephyrproject.org/latest/boards/arm/nrf52840_blip/doc/index.html).

Please follow the [getting started guide](https://docs.zephyrproject.org/latest/getting_started/index.html) and install
Zephyr and Zephyr toolchain etc. for your operating system. Once you have done that, you can
build this sample as follows.

```bash
$ . ~/repos/zephyrproject/zephyr/zephyr-env.sh
$ cd /path/to/this/directory/
$ ls
build  CMakeLists.txt  nrf52840_blip.overlay  prj.conf  README.md  reference  src
$ mkdir build && cd build
$ cmake -GNinja -DBOARD=nrf52840_blip ..
Zephyr version: 1.14.0
-- Found PythonInterp: /usr/bin/python3 (found suitable version "3.6.8", minimum required is "3.4") 
-- Selected BOARD nrf52840_blip
-- Found west: /home/tavish/.local/bin/west (found suitable version "0.5.7", minimum required is "0.5.6")
-- Loading /home/tavish/repos/zephyrproject/zephyr/boards/arm/nrf52840_blip/nrf52840_blip.dts as base
-- Overlaying /home/tavish/repos/zephyrproject/zephyr/dts/common/common.dts
-- Overlaying /home/tavish/repos/el/products/blip/blip-firmware/code/zephyr/tftm018/nrf52840_blip.overlay
nrf52840_blip.dts.pre.tmp:312.23-315.5: Warning (simple_bus_reg): /soc/virtualcom: missing or empty reg/ranges property
Parsing Kconfig tree in /home/tavish/repos/zephyrproject/zephyr/Kconfig
Loading /home/tavish/repos/zephyrproject/zephyr/boards/arm/nrf52840_blip/nrf52840_blip_defconfig as base
Merging /home/tavish/repos/el/products/blip/blip-firmware/code/zephyr/tftm018/prj.conf
Configuration written to '/home/tavish/repos/el/products/blip/blip-firmware/code/zephyr/tftm018/build/zephyr/.config'
-- Cache files will be written to: /home/tavish/.cache/zephyr
-- The C compiler identification is GNU 8.3.0
-- The CXX compiler identification is GNU 8.3.0
-- The ASM compiler identification is GNU
-- Found assembler: /opt/zephyr-sdk/arm-zephyr-eabi/bin/arm-zephyr-eabi-gcc
-- Performing Test toolchain_is_ok
-- Performing Test toolchain_is_ok - Success
Including module: tinycbor in path: /home/tavish/repos/zephyrproject/modules/lib/tinycbor
-- Configuring done
-- Generating done
-- Build files have been written to: /home/tavish/repos/el/products/blip/blip-firmware/code/zephyr/tftm018/build
```

Now, you can build and flash it using `ninja`, and `ninja flash` commands.

``` bash
$ $ ninja
[0/1] Re-running CMake...
Zephyr version: 1.14.0
-- Selected BOARD nrf52840_blip
-- Found west: /home/tavish/.local/bin/west (found suitable version "0.5.7", minimum required is "0.5.6")
-- Loading /home/tavish/repos/zephyrproject/zephyr/boards/arm/nrf52840_blip/nrf52840_blip.dts as base
-- Overlaying /home/tavish/repos/zephyrproject/zephyr/dts/common/common.dts
-- Overlaying /home/tavish/repos/el/products/blip/blip-firmware/code/zephyr/tftm018/nrf52840_blip.overlay
nrf52840_blip.dts.pre.tmp:312.23-315.5: Warning (simple_bus_reg): /soc/virtualcom: missing or empty reg/ranges property
Parsing Kconfig tree in /home/tavish/repos/zephyrproject/zephyr/Kconfig
Loading /home/tavish/repos/el/products/blip/blip-firmware/code/zephyr/tftm018/build/zephyr/.config as base
Configuration written to '/home/tavish/repos/el/products/blip/blip-firmware/code/zephyr/tftm018/build/zephyr/.config'
-- Cache files will be written to: /home/tavish/.cache/zephyr
Including module: tinycbor in path: /home/tavish/repos/zephyrproject/modules/lib/tinycbor
-- Configuring done
-- Generating done
-- Build files have been written to: /home/tavish/repos/el/products/blip/blip-firmware/code/zephyr/tftm018/build
[106/111] Linking C executable zephyr/zephyr_prebuilt.elf
Memory region         Used Size  Region Size  %age Used
           FLASH:       36952 B         1 MB      3.52%
            SRAM:       50280 B       256 KB     19.18%
        IDT_LIST:          88 B         2 KB      4.30%
[111/111] Linking C executable zephyr/zephyr.elf
$ ninja flash
[0/1] Flashing nrf52840_blip
Using runner: blackmagicprobe
Remote debugging using /dev/ttyACM0
Target voltage: unknown
Available Targets:
No. Att Driver
 1      Nordic nRF52
 2      Nordic nRF52 Access Port
Attaching to Remote target
warning: No executable has been specified and target does not support
determining executable automatically.  Try using the "file" command.
0x00003f20 in ?? ()
Loading section text, size 0x100 lma 0x0
Loading section _TEXT_SECTION_NAME_2, size 0x70e6 lma 0x100
Loading section .ARM.exidx, size 0x8 lma 0x71e8
Loading section sw_isr_table, size 0x180 lma 0x71f0
Loading section devconfig, size 0xb4 lma 0x7370
Loading section log_const_sections, size 0x40 lma 0x7424
Loading section log_backends_sections, size 0x10 lma 0x7464
Loading section rodata, size 0x17ec lma 0x7474
Loading section datas, size 0x330 lma 0x8c60
Loading section initlevel, size 0xb4 lma 0x8f90
Loading section _k_mutex_area, size 0x14 lma 0x9044
Start address 0x4098, load size 36950
Transfer rate: 20 KB/sec, 803 bytes/write.
[Inferior 1 (Remote target) killed]
```

Our LCD's driver chip, ILI9163, is configured to to work on 3-wire
SPI mode. Although Zephyr does have a display susbsystem, we are
going to use SPI directly to talk to the display. We use `SPI_3` instance of
SPI_1, because nRF52840 supports clock speeds of up to 32MHz on it, other
instances go up to 8), and we want a faster clock to draw on the screen faster,
so we have configured the clock to be running at 32MHz.

Zephyr already has driver code for reading from the thermal grid sensor
AMG8833, we use the sensor API in Zephyr to read sensor data.

Code repository
==========

* <i class="fa fa-git fa-1x" style="color: black"></i> [Blip thermal grid example](https://gitlab.com/electronutlabs-public/ElectronutLabs-blip/tree/master/code/projects/thermal_grid_lcd)

Files in this sample:

```
├── CMakeLists.txt		- Zephyr projects use CMake for building
├── nrf52840_blip.overlay	- Device tree overlay - SPI in this file here can be overridden in code
├── prj.conf			- Zephyr project config
├── README.md			- This file
└── src
    ├── arm_math.h		- from CMSIS, for arm SIMD interpolation functions
    ├── ili9163_spi.c		- LCD driver code
    ├── ili9163_spi.h
    ├── interpolated_colors.h	- pre generated colors for mapping temperature
    ├── lcdif.c			- SPI and gpio functions specific to zephyr/blip are in here
    ├── lcdif.h
    └── main.c			- application entry
```
