Blip default firmware
=====================

![app_blip](app_blip.png)

The default firmware collects data from all onboard sensors, temperature/humidity, light, and accelerometer and sends it over BLE(bluetooth low energy). The Electronut Labs mobile app can be used to connect to the device and see the sensor data. Refer to the video at the end of this section.

The Instruction for uploading default firmware to Blip and observing sensor data on the phone are explained in the sections below.

Build and Flash firmware
------------------------

The default firmware is built on the [Zephyr](https://docs.zephyrproject.org/latest/) rtos. The toolchain and SDK for Zephyr is required to build and upload the firmware. Follow the setup instructions [here](https://docs.zephyrproject.org/latest/getting_started/index.html).

> **Pre-built firmware**: Download prebuilt hex file from: <br><br>
> <i class="fa fa-file fa-1x" style="color: black"></i> [Default firmware .hex file](hex/default_firmware.hex)

If you are on Ubuntu or similar OS, there is a zip file with a python script to
do the flashing for you, that you may be able to use to quickly flash the
firmware to blip.

* <i class="fa fa-file fa-1x" style="color: black"></i> [flash_blip.zip](flashing_default_fw/flash_blip.zip)

Dependencies which need to be installed to run `python3 flash.py` inside this zip are:

    sudo apt install python3-serial setserial gcc-arm-embedded

Once you extract this zip file somewhere and have installed the dependencies,
simply run `python3 flash.py` in a terminal window.

For general instructions on how to flash firmware using Bumpy or blackmagicprobe,
Please see our [getting started guide](https://docs.electronut.in/blip/user-guide/#using-swd-debugger-for-external-chips). Further reading on Black Magic Probe usage:

- [Black Magic Probe - Getting started](https://github.com/blacksphere/blackmagic/wiki/Getting-Started)
- [Bumpy - using with Bluey](https://docs.electronut.in/bumpy/#bluey)

**Note:**

No special build instructions, except that for now, `Zephyr` needs a certain
branch, currently at `https://github.com/electronut/zephyr.git`.

This branch is based off `Zephyr` release v1.14, we only added drivers code for
which is not yet in mainline zephyr (it's in the process of being merged). To
use this branch, you will need Zephyr SDK v0.10.0.

To get to this branch:

```
cd /path/to/zephyr/
git remote add electronut https://github.com/electronut/zephyr.git
git fetch --all
git checkout electronut/blip_dev
```

Follow the command line instructions below to **Build the firmware and flash**
to the Blip.

```
cd /path/to/default_fw
git submodule init && git submodule update  # for nanopb
. /path/to/zephyr/zephyr-env.sh
mkdir build
cd build
cmake -GNinja -DBOARD=nrf52840_blip ..
ninja flash
```
The blip device will now advertise the sensor data which can be observed using Electronut Labs mobile app.


Sensors Data on Web Bluetooth demo
----------------------------------
You can talk directly to Blip using [Web Bluetooth](https://webbluetoothcg.github.io/web-bluetooth/). For this to work, your browser must support Web Bluetooth and the blip board should have the default firmware. To check list of available browsers, visit this [link](https://developer.mozilla.org/en-US/docs/Web/API/Web_Bluetooth_API#Browser_compatibility). On Google Chrome, you should have enabled experimental web features by visiting the url :
```chrome://flags/#enable-experimental-web-platform-features```

![open_demo](blip_wb.png)

* [See Demo Here](../sensor_web_bluetooth/)


Sensors Data on Mobile App
--------------------------
The Electronut Labs mobile app is available here:

* [Android App](https://play.google.com/store/apps/details?id=in.electronut.app&hl=en_US)

Here are the steps to get connected with the blip device after downloading the app.

1. Open the app and go to blip

![open_app](blip_app1.png)

2. Scan for any blip device, if it is advertising then you can see it like this below.

![scan](blip_app2.png)

3. Connect to the device. The sensor values will be displayed which will be consist of values of temperature, humidity, lux and accelerometer as shown below. 

![app_display](blip_app3.png)

**Observation**

If you move the blip device you will see the change in accelerometer values along x,y and z axis. Similar changes can be seen for lux and other sensor values.

Indrium Data Format
-------------------
We are using something we are calling [Indrium](https://gitlab.com/electronutlabs-public/indrium-schema) as our specification for the data format for sending sensor data.  

## Indrium Protobuf Format

Indrium is a protobuf spec we are using for sensor data. Protobuf is a serialization
format and it doesn't say anything about the transport layer (which could be BLE, or serial, or TCP etc.).
Indrium spec contains BLE transport layer requirements also, like the UUIDs of the service and characteristec, and
how large packets will be divided into smaller chunks (fragmentation).

Serialization using protobuf is structured data which is easy to parse
using libraries generated with protobuf tools, so we don't have to manually
parse the messages. In firmware we use nanopb library for parsing/generating
protobuf messages.

The linked document contains information about BLE specific portions of the
protocol also. Please see: [BLE transport specification](https://gitlab.com/electronutlabs-public/indrium-schema#ble-transport-specification).

Video
-----
Here is the [video](https://www.youtube.com/watch?v=Y8x6cwEgswc) of the default firmware in action.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Y8x6cwEgswc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Code Repository
---------------

The source for can be found on gitlab here.

* <i class="fa fa-git fa-1x" style="color: black"></i> [Blip default firmware](https://gitlab.com/electronutlabs-public/ElectronutLabs-blip/tree/master/code/default_fw)

