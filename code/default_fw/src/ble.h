/** @file module.h
 *
 * @brief sensor data BLE service.  
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Electronut Labs.
 * All rights reserved. 
*/
 
#ifndef _MODULE_H
#define _MODULE_H
 
#ifdef __cplusplus 
extern "C" { 
#endif

#include <zephyr.h>
#include <settings/settings.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>
#include "common.h"

#define MAX_ADV_SIZE 18

void ble_init(void);

void ble_notify(uint8_t *buffer, uint8_t len);

#ifdef __cplusplus 
}
#endif
 
#endif /* _MODULE_H */