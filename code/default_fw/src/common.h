#ifndef __COMMON_H__
#define __COMMON_H__

#include <zephyr.h>
#include <sensor.h>
#include <stdint.h>

enum {
	MSG_TYPE_LOGGING_ENABLE,
	MSG_TYPE_LOGGING_DISABLE,
	MSG_TYPE_SAMPLE
};

struct sensor_data
{
	uint8_t type; // see above enum
	struct sensor_value accel[3];
	struct sensor_value brightness;
	struct sensor_value temp;
	struct sensor_value humi;
};

#endif