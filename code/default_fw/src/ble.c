/** @file module.c
 *
 * @brief sensor data BLE service.
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Electronut Labs.
 * All rights reserved.
*/
#include "common.h"
#include "ble.h"

/* Custom Service Variables */
//def9bda9-4370-469c-98d0-7986a8b64034
static struct bt_uuid_128 vnd_uuid = BT_UUID_INIT_128(
	0x34, 0x40, 0xb6, 0xa8, 0x86, 0x79, 0xd0, 0x98,
	0x9c, 0x46, 0x70, 0x43, 0xa9, 0xbd, 0xf9, 0xde);

//def9bda9-4370-469c-98d0-7986a8b64035
static struct bt_uuid_128 vnd_enc_uuid = BT_UUID_INIT_128(
	0x35, 0x40, 0xb6, 0xa8, 0x86, 0x79, 0xd0, 0x98,
	0x9c, 0x46, 0x70, 0x43, 0xa9, 0xbd, 0xf9, 0xde);

static u8_t vnd_value[18] = {'0'};
//static u8_t number = 0U;

static ssize_t read_vnd(struct bt_conn *conn, const struct bt_gatt_attr *attr,
	void *buf, u16_t len, u16_t offset)
{
	const char *value = attr->user_data;

	printk("value read : %s \n", value);

	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
		strlen(value));
}

static ssize_t write_vnd(struct bt_conn *conn, const struct bt_gatt_attr *attr,
	const void *buf, u16_t len, u16_t offset, u8_t flags)
{
	u8_t *value = attr->user_data;

	if (offset + len > sizeof(vnd_value)) {
		printk("write error\n");
		return BT_GATT_ERR(BT_ATT_ERR_INVALID_OFFSET);
	}

	memcpy(value + offset, buf, len);

	for (int i = 0; i < len; i++) {
		printk("%02x", value[i]);
	}
	printk("\n");

	return len;
}

static struct bt_gatt_ccc_cfg vnd_ccc_cfg[BT_GATT_CCC_MAX] = {};
static u8_t is_notify_enabled;

static void vnd_ccc_cfg_changed(const struct bt_gatt_attr *attr, u16_t value)
{
	is_notify_enabled = value == BT_GATT_CCC_NOTIFY;
}

/* Vendor Primary Service Declaration */
static struct bt_gatt_attr vnd_attrs[] = {
	/* Vendor Primary Service Declaration */
	BT_GATT_PRIMARY_SERVICE(&vnd_uuid),
	BT_GATT_CHARACTERISTIC(&vnd_enc_uuid.uuid,
		BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE |
			BT_GATT_CHRC_NOTIFY,
		BT_GATT_PERM_READ | BT_GATT_PERM_WRITE,
		read_vnd, write_vnd, vnd_value),
	BT_GATT_CCC(vnd_ccc_cfg, vnd_ccc_cfg_changed),
};

static struct bt_gatt_service vnd_svc = BT_GATT_SERVICE(vnd_attrs);

static void send_adv_data(uint8_t *adv_data, uint8_t len)
{

	bt_gatt_notify(NULL, &vnd_attrs[1], adv_data, len);
}

static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR))};

static void connected(struct bt_conn *conn, u8_t err)
{
	if (err) {
		printk("Connection failed (err %u)\n", err);
	}
	else {
		printk("Connected\n");
	}
}

static void disconnected(struct bt_conn *conn, u8_t reason)
{
	printk("Disconnected (reason %u)\n", reason);
}

static struct bt_conn_cb conn_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
};

static void bt_ready(int err)
{
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	printk("Bluetooth initialized\n");

	bt_gatt_service_register(&vnd_svc);

	if (IS_ENABLED(CONFIG_SETTINGS)) {
		settings_load();
	}

	err = bt_le_adv_start(BT_LE_ADV_CONN_NAME, ad, ARRAY_SIZE(ad), NULL, 0);
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}

	printk("Advertising successfully started\n");
}

void ble_init(void)
{
	int err;

	err = bt_enable(bt_ready);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	bt_conn_cb_register(&conn_callbacks);
}

void ble_notify(uint8_t *buffer, uint8_t len)
{
	/* Advertising data packet, keep count of remaining data in buffer
	   and current data item in buffer */
	uint8_t adv_data[20];
	int remaining;
	int current_len;
	uint16_t frame_count;

	remaining = len;
	current_len = 0;
	frame_count = 0;

	/* if the notification data is enabled, keep filling the adv data so that
	   it do not exceed 20 bytes*/
	if (is_notify_enabled) {
		while (remaining > 0) {
			/* MAX_ADV_SIZE is the maximum data can be accomodated in adv
			   packet after frame_count */
			int to_send = MIN(MAX_ADV_SIZE, remaining);
			memcpy(&adv_data[2], &buffer[current_len], to_send);
			remaining = remaining - to_send;
			if (remaining > 0) {
				current_len = current_len + to_send;
				frame_count = frame_count + 1;
			}
			else {
				frame_count = 0;
			}
			adv_data[0] = (frame_count & 0xff);
			adv_data[1] = (frame_count >> 8);
			send_adv_data(adv_data, to_send + 2);
		}
	}
}