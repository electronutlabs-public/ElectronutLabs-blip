/*
 * Copyright (c) Electronut Labs.
 *
 */
#include <zephyr.h>
#include <misc/printk.h>
#include <sensor.h>
#include <stdio.h>
#include <disk_access.h>
#include <fs.h>
#include <ff.h>
#include <gpio.h>

#include "ble.h"
#include "common.h"
#include "nanopb/pb_encode.h"
#include "indrium.pb.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(blip_fw, LOG_LEVEL_DBG);

static struct device *dev_lis;
static struct device *dev_ltr;
static struct device *dev_si7006;

K_MSGQ_DEFINE(logging_q, sizeof(struct sensor_data), 10, 4);
K_MSGQ_DEFINE(ble_q, sizeof(struct sensor_data), 10, 4);

static void sample_fn(void);
static void logging_fn();

K_THREAD_DEFINE(logging_id, 512, logging_fn, NULL, NULL, NULL,
	10, 0, K_NO_WAIT);

K_THREAD_DEFINE(sample_id, 512, sample_fn, NULL, NULL, NULL,
	10, 0, K_NO_WAIT);

static void button_interrupt_callback(struct device *dev, 
	struct gpio_callback *cb, u32_t pins)
{
	struct sensor_data val;

	printk("button pressed\r\n");

	static bool flag_logging_toggle = false;
	if (flag_logging_toggle == false) {
		flag_logging_toggle = true;
		val.type = MSG_TYPE_LOGGING_ENABLE;
		while (k_msgq_put(&logging_q, &val, K_NO_WAIT) != 0) {
			/* message queue is full: purge old data & try again */
			k_msgq_purge(&logging_q);
		}
	}
	else {
		flag_logging_toggle = false;
		val.type = MSG_TYPE_LOGGING_DISABLE;
		while (k_msgq_put(&logging_q, &val, K_NO_WAIT) != 0) {
			/* message queue is full: purge old data & try again */
			k_msgq_purge(&logging_q);
		}
	}
}

static void sample_fn(void)
{
	struct sensor_data val;

	while (1) {
		sensor_sample_fetch(dev_lis);
		sensor_sample_fetch(dev_ltr);
		sensor_sample_fetch(dev_si7006);

		sensor_channel_get(dev_lis, SENSOR_CHAN_ACCEL_XYZ, val.accel);
		sensor_channel_get(dev_ltr, SENSOR_CHAN_LIGHT, &val.brightness);
		sensor_channel_get(dev_si7006, SENSOR_CHAN_HUMIDITY, &val.humi);
		sensor_channel_get(dev_si7006, SENSOR_CHAN_AMBIENT_TEMP, &val.temp);
		val.type = MSG_TYPE_SAMPLE;
		
		/* send data to logging thread */
		while (k_msgq_put(&logging_q, &val, K_NO_WAIT) != 0) {
			/* message queue is full: purge old data & try again */
			k_msgq_purge(&logging_q);
		}
		/* send data to main thread */
		while (k_msgq_put(&ble_q, &val, K_NO_WAIT) != 0) {
			/* message queue is full: purge old data & try again */
			k_msgq_purge(&ble_q);
		}

		k_sleep(200);
	}
}

static void logging_fn()
{
	struct sensor_data val;

	bool logging_enabled = false;
	bool mounted = false;

	char output_buffer[100];

	/*
	*  Note the fatfs library is able to mount only strings inside _VOLUME_STRS
	*  in ffconf.h
	*/
	static const char *disk_mount_pt = "/SD:";
	static FATFS fat_fs;
	static struct fs_file_t file;
	/* mounting info */
	static struct fs_mount_t mp = {
		.type = FS_FATFS,
		.fs_data = &fat_fs,
	};

	while (1) {
		k_msgq_get(&logging_q, &val, K_FOREVER);

		switch (val.type) {
		case MSG_TYPE_LOGGING_ENABLE:
			// mount and open file here
			mp.mnt_point = disk_mount_pt;
			int res = fs_mount(&mp);

			if (res == FR_OK) {
				printk("Disk mounted.\n");
				mounted = true;
			}
			else {
				printk("Error mounting disk.\n");
				mounted = false;
			}

			k_sleep(3000);

			int rc = fs_open(&file, "/SD:/myfile.csv");
			if (rc != 0) {
				LOG_ERR("file not created\n");
				mounted = false;
			}
			logging_enabled = true;
			printk("logging_enabled\r\n");
			while(k_msgq_get(&logging_q, &val, K_NO_WAIT) == 0);

			break;

		case MSG_TYPE_LOGGING_DISABLE:
			// close file and unmount here
			printk("logging disabled\r\n");
			if(logging_enabled) {
				if (fs_close(&file) != 0) {
					LOG_ERR("file not closed");
				}
				logging_enabled = false;
			}
			if(mounted) {
				if (fs_unmount(&mp) != 0) {
					LOG_ERR("Unable to unmount");
				}
				mounted = false;
			}
			break;

		case MSG_TYPE_SAMPLE:
			if (logging_enabled && mounted) {
				int written =
					snprintf(output_buffer, sizeof(output_buffer)
					- 1, "%10.2f, %10.2f, %10.2f, %10.2f, "
					"%10.2f, %10.2f\n",
					sensor_value_to_double(&val.accel[0]),
					sensor_value_to_double(&val.accel[1]),
					sensor_value_to_double(&val.accel[2]),
					sensor_value_to_double(&val.brightness),
					sensor_value_to_double(&val.humi),
					sensor_value_to_double(&val.temp));

				output_buffer[sizeof(output_buffer) - 1] = 0;
				int w_size = fs_write(&file, output_buffer, written);

				if (w_size > 0) {
					LOG_DBG("%d bytes written\r\n", w_size);
				}
				else  {
					LOG_ERR("err in writing");
				}
			}
			break;
		default:
			break;
		}
	}
}

/*
 * @brief get sensor data to the message queue and encode data.
 */
static uint8_t sensor_data_encode(u8_t *buffer, struct sensor_data val)
{
	indrium_IndriumTx blip_sensor_proto = indrium_IndriumTx_init_default;

	/* fill up the proto buffer to encode */
	blip_sensor_proto.has_timestamp = false;
	blip_sensor_proto.has_DeviceID = false;
	blip_sensor_proto.sensor_count = 6;
	blip_sensor_proto.sensor[0].which_val = indrium_Sensor_tFloat_tag;
	blip_sensor_proto.sensor[0].type = indrium_SensorType_TYPE_TEMPERATURE;
	blip_sensor_proto.sensor[0].val.tFloat = sensor_value_to_double(&val.temp);
	blip_sensor_proto.sensor[1].which_val = indrium_Sensor_tFloat_tag;
	blip_sensor_proto.sensor[1].type = indrium_SensorType_TYPE_RELATIVE_HUMIDITY;
	blip_sensor_proto.sensor[1].val.tFloat = sensor_value_to_double(&val.humi);
	blip_sensor_proto.sensor[2].which_val = indrium_Sensor_tFloat_tag;
	blip_sensor_proto.sensor[2].type = indrium_SensorType_TYPE_BRIGHTNESS;
	blip_sensor_proto.sensor[2].val.tFloat = sensor_value_to_double(&val.brightness);
	blip_sensor_proto.sensor[3].which_val = indrium_Sensor_tFloat_tag;
	blip_sensor_proto.sensor[3].type = indrium_SensorType_TYPE_ACCELERATION;
	blip_sensor_proto.sensor[3].instanceID = 1;
	blip_sensor_proto.sensor[3].val.tFloat = sensor_value_to_double(&val.accel[0]);
	blip_sensor_proto.sensor[4].instanceID = 2;
	blip_sensor_proto.sensor[4].which_val = indrium_Sensor_tFloat_tag;
	blip_sensor_proto.sensor[4].type = indrium_SensorType_TYPE_ACCELERATION;
	blip_sensor_proto.sensor[4].val.tFloat = sensor_value_to_double(&val.accel[1]);
	blip_sensor_proto.sensor[5].instanceID = 3;
	blip_sensor_proto.sensor[5].which_val = indrium_Sensor_tFloat_tag;
	blip_sensor_proto.sensor[5].type = indrium_SensorType_TYPE_ACCELERATION;
	blip_sensor_proto.sensor[5].val.tFloat = sensor_value_to_double(&val.accel[2]);
	blip_sensor_proto.has_resp = false;
	/* encode the proto buffer */
	pb_ostream_t stream = pb_ostream_from_buffer(buffer, indrium_IndriumTx_size);
	pb_encode(&stream, indrium_IndriumTx_fields, &blip_sensor_proto);
	size_t message_length = stream.bytes_written;
	uint8_t length = message_length;
	/* display data : if debug log is on*/
	// LOG_DBG("encoded size: %d \n", message_length);
	// LOG_HEXDUMP_DBG(buffer, message_length, "Encoded: ");

	return length;
}

static void setup_button_int()
{
	/* gpio initialisation*/
	struct device *gpio_dev = device_get_binding(SW0_GPIO_CONTROLLER);
	static struct gpio_callback gpio_cb;

	if (gpio_dev == NULL) {
		printk("Failed to get pointer device!");
		return;
	}

	gpio_pin_configure(gpio_dev, SW0_GPIO_PIN, GPIO_DIR_IN | GPIO_INT | 
		GPIO_PUD_PULL_UP | GPIO_INT_EDGE | GPIO_INT_DEBOUNCE);

	gpio_init_callback(&gpio_cb, button_interrupt_callback,
		BIT(SW0_GPIO_PIN));

	if (gpio_add_callback(gpio_dev, &gpio_cb) < 0) {
		printk("Failed to set button interrupt callback!");
	}
	else {
		gpio_pin_enable_callback(gpio_dev, SW0_GPIO_PIN);
	}
}

void main()
{
	printk("Welcome! blip default firmware");

	ble_init();

	/* initialise on board sensors*/
	dev_lis = device_get_binding(DT_ST_LIS2DH_0_LABEL);
	if (dev_lis == NULL) {
		LOG_INF("Could not get %s device\n", DT_ST_LIS2DH_0_LABEL);
		return;
	}
	dev_ltr = device_get_binding("LTR_0");
	if (dev_ltr == NULL) {
		LOG_INF("Could not get light sensor device\r\n");
		return;
	}
	dev_si7006 = device_get_binding("SI7006_0");
	if (dev_si7006 == NULL) {
		LOG_INF("Could not get SI7006 device\r\n");
		return;
	}
	uint8_t len;

	setup_button_int();

	struct sensor_data val;
	// large, don't want it on stack
	static uint8_t buffer[indrium_IndriumTx_size];
	while (1) {
		/* get sensor data from sample thread */
		k_msgq_get(&ble_q, &val, K_FOREVER);
		/* encode the data and send */
		len = sensor_data_encode(&buffer[0], val);
		ble_notify(buffer, len);
	}
}
