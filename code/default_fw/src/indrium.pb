
�
indrium.protoindrium"8

PapyrChunk
offset (Roffset
data (Rdata"�
Sensor'
type (2.indrium.SensorTypeRtype!

instanceID (:1R
instanceID
tInt (H RtInt
tFloat (H RtFloatB
val"9

IndriumCmd+
type (2.indrium.IndriumCmdTypeRtype"p
IndriumCmdResponse
intNum (H RintNum
floatNum (H RfloatNum
asBytes (H RasBytesB
type"�
	IndriumTx
	timestamp (R	timestamp
DeviceID (RdeviceID'
sensor (2.indrium.SensorRsensor/
resp (2.indrium.IndriumCmdResponseRresp"i
	IndriumRx+
chunk (2.indrium.PapyrChunkH Rchunk'
cmd (2.indrium.IndriumCmdH RcmdB
type*�

SensorType
TYPE_TEMPERATURE 
TYPE_MAGNETICFIELD
TYPE_DISTANCE
TYPE_BRIGHTNESS
TYPE_RELATIVE_HUMIDITY
TYPE_ACCELERATION
TYPE_ORIENTATION
TYPE_GYROSCOPE
TYPE_VOLTAGE
TYPE_CURRENT	*�
IndriumCmdType
CMD_PAPYR_MSG_END 
CMD_PAPYR_MSG_START_RLE"
CMD_PAPYR_MSG_START_HEATSHRINK
CMD_PAPYR_LAST_OFFSETBH