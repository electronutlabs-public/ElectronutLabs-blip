#!/usr/bin/python3
import sys
import subprocess
import time
import serial
import re

import logging

"""
Depedencies for Ubuntu: "sudo apt install python3-serial setserial gcc-arm-embedded"
"""

blip_logger = logging.getLogger(__name__)
blip_logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(fmt='[%(asctime)s]     %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')

blip_logger_fh = logging.FileHandler('/tmp/blip_prod_test.log')
blip_logger_fh.setLevel(logging.DEBUG)
blip_logger_fh.setFormatter(formatter)
blip_logger.addHandler(blip_logger_fh)
blip_logger.addHandler(logging.StreamHandler(sys.stdout))

blip_logger.info("Programming blip with default firmware")

##########################################################
    
def flash_test_firmware_callback():
    blip_logger.info("Detecting blip...")
    
    detect_blip = subprocess.Popen(["setserial -g /dev/ttyACM[0123456789]"], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    detect_blip_out, detect_blip_err = detect_blip.communicate()
    
    blip_logger.info("------------------------------------------------")
    if detect_blip_out:
        blip_logger.info("standard output of detect_blip:")
        blip_logger.info(detect_blip_out)
        r_ttyACM = re.compile('/dev/ttyACM.')
        ports = r_ttyACM.findall(detect_blip_out)
        blip_logger.info('Blip detected')
        blip_logger.info("ports: ")
        blip_logger.info(ports)
        
        blip_logger.info(">>> erasing...")
        erase_command = "arm-none-eabi-gdb -nx --batch -ex 'target extended-remote "+ ports[0] + "' -x erase_blip.scr "
        subprocess.call(erase_command + " > /dev/null 2>&1", shell=True)

        flash_command = "arm-none-eabi-gdb -nx --batch -ex 'target extended-remote "+ ports[0] + "' -x flash_blip.scr "
        blip_logger.info(">>> flashing default firmware...")
        flash_blip_test_firmware = subprocess.Popen([flash_command+"default_firmware.hex"], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        flash_blip_test_firmware_out, flash_blip_test_firmware_err = flash_blip_test_firmware.communicate()
        
        # Remote communication error.  Target disconnected.: Input/output error.
        # Ignoring packet error, continuing...
        # SW-DP scan failed!
        # Attaching to Remote target failed
        
        # /home/pi/blip_prod_test/flash_blip.scr:4: Error in sourced command file:
        # Remote communication error.  Target disconnected.: Input/output error.

        if ((flash_blip_test_firmware_err.find('SW-DP scan failed!')!= -1) or
            (flash_blip_test_firmware_err.find('Ignoring packet error, continuing...')!= -1) or
            (flash_blip_test_firmware_err.find('Remote communication error.')!= -1)):
            blip_logger.info("Error flashing test firmware")
            blip_logger.info("Production test has failed")
            
        else:
            blip_logger.info("Flashed default firmware")
            blip_logger.info("You should now power cycle blip once")
            
            
        blip_logger.info("------------------------------------------------")
        if flash_blip_test_firmware_out:
            blip_logger.info("standard output of flash_blip_test_firmware:")
            blip_logger.info(flash_blip_test_firmware_out)
        if flash_blip_test_firmware_err:
            blip_logger.info("standard error of flash_blip_test_firmware:")
            blip_logger.info(flash_blip_test_firmware_err)
        blip_logger.info("returncode of flash_blip_test_firmware:")
        blip_logger.info(flash_blip_test_firmware.returncode)
        blip_logger.info("------------------------------------------------")
        
    if detect_blip_err:
        blip_logger.info("standard error of detect_blip:")
        blip_logger.info(detect_blip_err)
        
        blip_logger.info("Blip not detected")
        blip_logger.info("Firmware flashing has failed")
    blip_logger.info("returncode of detect_blip:")
    blip_logger.info(detect_blip.returncode)
    blip_logger.info("------------------------------------------------")

if __name__ == "__main__":
    flash_test_firmware_callback()