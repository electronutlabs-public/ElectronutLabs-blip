/**
 * @file main.c
 * @author Ajay Meena (ajay@electronut.in)
 * @brief Blip Sensors+ePaper firmware
 * 
 * @copyright Copyright (c) 2019, Electronut Labs (electronut.in)
 * 
 */
#include <zephyr.h>
#include <misc/printk.h>
#include <sensor.h>
#include <stdio.h>
#include <stdint.h>

#include <device.h>
#include <gpio.h>
#include <spi.h>

#include "epd1in54b.h"
#include "epdif.h"
#include "epdpaint.h"
#include "image.h"
#include "fonts.h"

// epaper shield mosfet pin
#define PIN_E_INK 4

static struct device *dev_lis;
static struct device *dev_ltr;
static struct device *dev_si7006;

// PORT0 instance
struct device * port0;

struct sensor_data
{
	struct sensor_value accel[3];
	struct sensor_value brightness;
	struct sensor_value temp;
	struct sensor_value humi;
};

// e-Paper structure
EPD epd;

// Frame buffer to store image
unsigned char frame_buffer_black_arr[EPD_WIDTH * EPD_HEIGHT / 8];
unsigned char *frame_buffer_black = frame_buffer_black_arr;
unsigned char frame_buffer_red_arr[EPD_WIDTH * EPD_HEIGHT / 8];
unsigned char *frame_buffer_red = frame_buffer_red_arr;

// Paint structure
Paint paint_black;
Paint paint_red;

// Initialize e-Paper display
int epaper_init()
{
    epaper_GPIO_Init();

    return EPD_Init(&epd);
}

// Initialize e-Paper paint/frame structure
void epaper_paint_init()
{
    Paint_Init(&paint_black, frame_buffer_black, epd.width, epd.height);
    Paint_Init(&paint_red, frame_buffer_red, epd.width, epd.height);
    Paint_Clear(&paint_black, UNCOLORED);
    Paint_Clear(&paint_red, UNCOLORED);
}

// Pull down epaper pins to save power
void epaper_pins_off()
{
	gpio_pin_configure(port0, EPD_BUSY_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_RST_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_DC_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_SPI_CS_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_SPI_SCK_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_SPI_MOSI_PIN, GPIO_DIR_OUT);

	gpio_pin_write(port0, EPD_BUSY_PIN, 0);
	gpio_pin_write(port0, EPD_RST_PIN, 0);
	gpio_pin_write(port0, EPD_DC_PIN, 0);
	gpio_pin_write(port0, EPD_SPI_CS_PIN, 0);
	gpio_pin_write(port0, EPD_SPI_SCK_PIN, 0);
	gpio_pin_write(port0, EPD_SPI_MOSI_PIN, 0);
}

// get sensor data
void get_sensor_data(struct sensor_data *val)
{
	sensor_sample_fetch(dev_lis);
	sensor_sample_fetch(dev_ltr);
	sensor_sample_fetch(dev_si7006);

	sensor_channel_get(dev_lis, SENSOR_CHAN_ACCEL_XYZ, val->accel);
	sensor_channel_get(dev_ltr, SENSOR_CHAN_LIGHT, &(val->brightness));
	sensor_channel_get(dev_si7006, SENSOR_CHAN_HUMIDITY, &(val->humi));
	sensor_channel_get(dev_si7006, SENSOR_CHAN_AMBIENT_TEMP, &(val->temp));
}

// display sensor data on e-Paper
void display_sensor_data(struct sensor_data val)
{
	char tempData[14];
	sprintf(tempData, "TEMP: %d.%02d", val.temp.val1, val.temp.val2/10000);
	char humiData[14];
	sprintf(humiData, "HUMI: %d.%02d", val.humi.val1, val.humi.val2/10000);
	char luxData[14];
	sprintf(luxData, "LUX : %d.%02d", val.brightness.val1, val.brightness.val2/10000);

	char accelXData[14];
	sprintf(accelXData, "X: %d.%02d", val.accel[0].val1, val.accel[0].val2/10000);
	char accelYData[14];
	sprintf(accelYData, "Y: %d.%02d", val.accel[1].val1, val.accel[1].val2/10000);
	char accelZData[14];
	sprintf(accelZData, "Z: %d.%02d", val.accel[2].val1, val.accel[2].val2/10000);

	// epaper display MOSFET
	gpio_pin_configure(port0, PIN_E_INK, GPIO_DIR_OUT);

	// turn on epaper display MOSFET
	gpio_pin_write(port0, PIN_E_INK, 0);

	// init epaper display
	epaper_init();
	epaper_paint_init();

	Paint_SetRotate(&paint_red, 3);
	Paint_SetRotate(&paint_black, 3);
	Paint_DrawStringAt(&paint_red, 10, 20, tempData , &Font20, COLORED);
	Paint_DrawStringAt(&paint_red, 10, 40, humiData , &Font20, COLORED);
	Paint_DrawStringAt(&paint_red, 10, 60, luxData , &Font20, COLORED);
	Paint_DrawStringAt(&paint_black, 10, 100, "ACCELERATION" , &Font20, COLORED);
	Paint_DrawStringAt(&paint_black, 60, 130, accelXData , &Font20, COLORED);
	Paint_DrawStringAt(&paint_black, 60, 150, accelYData, &Font20, COLORED);
	Paint_DrawStringAt(&paint_black, 60, 170, accelZData , &Font20, COLORED);
	EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);

	// turn off epaper display MOSFET
	gpio_pin_write(port0, PIN_E_INK, 1);
	epaper_pins_off();
}

void main(void)
{
	struct sensor_data val;
	s64_t time_stamp;

	/* initialise on board sensors*/
	dev_lis = device_get_binding(DT_ST_LIS2DH_0_LABEL);
	if (dev_lis == NULL) {
		printk("Could not get %s device\n", DT_ST_LIS2DH_0_LABEL);
		return;
	}
	dev_ltr = device_get_binding("LTR_0");
	if (dev_ltr == NULL) {
		printk("Could not get light sensor device\r\n");
		return;
	}
	dev_si7006 = device_get_binding("SI7006_0");
	if (dev_si7006 == NULL) {
		printk("Could not get SI7006 device\r\n");
		return;
	}

	port0 = device_get_binding("GPIO_0");
	
	while(1)
	{
		
		get_sensor_data(&val);

		/* capture time stamp */
		time_stamp = k_uptime_get()/1000;
		printk("time_stamp : %lld\n", time_stamp);
		printk("TEMP: %d.%02d\n", val.temp.val1, val.temp.val2/10000);
		printk("HUMI: %d.%02d\n", val.humi.val1, val.humi.val2/10000);
		printk("LUX: %d.%02d\n", val.brightness.val1, val.brightness.val2/10000);
		printk("ACCEL_XYZ %d.%02d %d.%02d %d.%02d\n", val.accel[0].val1, val.accel[0].val2/10000, \
								val.accel[1].val1, val.accel[1].val2/10000, \
								val.accel[2].val1, val.accel[2].val2/10000);

		display_sensor_data(val);
		printk("-------------------------------------\n");
		k_sleep(30000);
	}
}
