import time
import board
from digitalio import DigitalInOut, Direction, Pull

led = DigitalInOut(board.D19)
led.direction = Direction.OUTPUT

while True:
	led.value = False
	time.sleep(0.1)
	led.value = True
	time.sleep(0.1)
