/*
  BLE MIDI sequencer example for Blip+Blipboy shield

  needs:
  https://github.com/tigoe/Button
  https://github.com/electronut/Adafruit_ILI9163
  https://github.com/FortySevenEffects/arduino_midi_library
  https://github.com/adafruit/Adafruit-GFX-Library

  Copyright (c) Electronut Labs
*/


#include <bluefruit.h>
#include <MIDI.h>
#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9163.h"
#include <Button.h>

BLEDis bledis;
BLEMidi blemidi;

// Create a new instance of the Arduino MIDI Library,
// and attach BluefruitLE MIDI as the transport.
MIDI_CREATE_BLE_INSTANCE(blemidi);

#define TFT_CS          4
#define TFT_DC          33
#define TFT_RESET       32
#define TFT_LEDK        19

Adafruit_ILI9163 tft = Adafruit_ILI9163(&SPI1, TFT_DC, TFT_CS, TFT_RESET);

Button b_up = Button(PIN_BUTTON_UP, BUTTON_PULLUP);
Button b_down = Button(PIN_BUTTON_DOWN, BUTTON_PULLUP);
Button b_right = Button(PIN_BUTTON_RIGHT, BUTTON_PULLUP);
Button b_left = Button(PIN_BUTTON_LEFT, BUTTON_PULLUP);
Button b_b = Button(PIN_BUTTON_B, BUTTON_PULLUP);

uint16_t loop_delay = 300;

void setup()
{
  Serial1.begin(115200);

  Serial1.println("MIDI sequencer example");

  // Config the peripheral connection with maximum bandwidth 
  // more SRAM required by SoftDevice
  // Note: All config***() function must be called before begin()
  Bluefruit.configPrphBandwidth(BANDWIDTH_MAX);  

  Bluefruit.begin();
  Bluefruit.setName("MIDI Sequencer");
  Bluefruit.setTxPower(4);    // Check bluefruit.h for supported values

  // Setup the on board blue LED to be enabled on CONNECT
  Bluefruit.autoConnLed(true);

  // Configure and Start Device Information Service
  bledis.setManufacturer("Electronut Labs");
  bledis.setModel("Blipboy");
  bledis.begin();

  // Initialize MIDI, and listen to all MIDI channels
  // This will also call blemidi service's begin()
  MIDI.begin(MIDI_CHANNEL_OMNI);

  // Attach the handleNoteOn function to the MIDI Library. It will
  // be called whenever the Bluefruit receives MIDI Note On messages.
  MIDI.setHandleNoteOn(handleNoteOn);

  // Do the same for MIDI Note Off messages.
  MIDI.setHandleNoteOff(handleNoteOff);
  
  // Set up and start advertising
  startAdv();

  // Start MIDI read loop
  Scheduler.startLoop(midiRead);

  // LCD Backlight
  pinMode(TFT_LEDK, OUTPUT);
  digitalWrite(TFT_LEDK, HIGH);

  tft.begin();
  draw_grid();
}

void startAdv(void)
{
  // Set General Discoverable Mode flag
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);

  // Advertise TX Power
  Bluefruit.Advertising.addTxPower();

  // Advertise BLE MIDI Service
  Bluefruit.Advertising.addService(blemidi);

  // Secondary Scan Response packet (optional)
  // Since there is no room for 'Name' in Advertising packet
  Bluefruit.ScanResponse.addName();

  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds  
}

void handleNoteOn(byte channel, byte pitch, byte velocity)
{
  // Log when a note is pressed.
  Serial1.printf("Note on: channel = %d, pitch = %d, velocity - %d", channel, pitch, velocity);
  Serial1.println();
}

void handleNoteOff(byte channel, byte pitch, byte velocity)
{
  // Log when a note is released.
  Serial1.printf("Note off: channel = %d, pitch = %d, velocity - %d", channel, pitch, velocity);
  Serial1.println();
}

void loop()
{
  // Don't continue if we aren't connected.
  if (! Bluefruit.connected()) {
    // return;
  }

  // Don't continue if the connected device isn't ready to receive messages.
  if (! blemidi.notifyEnabled()) {
    // return;
  }

  process_input();
  
  static uint32_t last_time = 0;
  uint32_t cur_time = millis();

  if((cur_time - last_time) > loop_delay) {
    send_current_column();
    update_grid();
    last_time = cur_time;
  }

  delay(10);

}

void midiRead()
{
  // Don't continue if we aren't connected.
  if (! Bluefruit.connected()) {
    return;
  }

  // Don't continue if the connected device isn't ready to receive messages.
  if (! blemidi.notifyEnabled()) {
    return;
  }

  // read any new MIDI messages
  MIDI.read();
}

bool notes[8][8] = {
  {1, 0, 0, 0, 0, 0, 0, 0}, 
  {0, 1, 0, 0, 0, 0, 0, 0}, 
  {0, 0, 0, 1, 0, 0, 0, 0}, 
  {0, 0, 0, 0, 1, 0, 0, 0}, 
  {0, 0, 0, 0, 0, 0, 1, 0}, 
  {0, 0, 0, 0, 0, 0, 0, 1}, 
  {1, 0, 0, 0, 0, 0, 0, 0}, 
  {0, 1, 0, 0, 0, 0, 0, 0}, 
};

// middle C is 60, increment by 1 is increment by 1 semitone, default is major scale
uint8_t note_values[8] = {
  60,
  62,
  64,
  65,
  67,
  69,
  71,
  72,
};

// bar is the green travelling line on top
static uint16_t current_bar_x = 0;

// red box on active one
static uint16_t current_active_x = 0;
static uint16_t current_active_y = 0;

void send_current_column() {
  // 
  static bool previous_notes[8] = {0}; //no active by default

  bool current_column[8];

  for(uint8_t i=0; i<8; i++) {
    current_column[i] = notes[i][current_bar_x];
  }

  // Serial1.print("Col: ");
  Serial1.println(current_bar_x);
  for(uint8_t i=0; i<8; i++) {
    if(!previous_notes[i] && current_column[i]) {
      MIDI.sendNoteOn(note_values[i], 127, 1);
      // Serial1.print("Play: ");
      // Serial1.println(i);
    }
    if(previous_notes[i] && !current_column[i]) {
      MIDI.sendNoteOff(note_values[i], 0, 1);
    }
  }
  
  for(uint8_t i=0; i<8; i++) {
    previous_notes[i] = current_column[i];
  }
}

void draw_grid()
{
  tft.fillScreen(ILI9163_BLACK);

  for(uint16_t x=0; x<8; x++) {
    for(uint16_t y=0; y<8; y++) {
      if(notes[y][x] == 0) {
        tft.drawRect(x*16, y*16, 16, 16, ILI9163_WHITE);
      }
      else {
        tft.drawRect(x*16, y*16, 16, 16, ILI9163_WHITE);
        tft.fillRect(x*16, y*16, 16, 16, ILI9163_WHITE);
      }
    }
  }
}

void update_grid()
{
  // reset previous top line
  tft.drawFastHLine(current_bar_x*16, 0, 16, ILI9163_WHITE);
  // draw block currently active
  tft.drawRect(current_active_x*16, current_active_y*16, 16, 16, ILI9163_RED);
  // update to next
  current_bar_x = (current_bar_x + 1) % 8;
  // change color to RED
  tft.drawFastHLine(current_bar_x*16, 0, 16, ILI9163_GREEN);
}

void process_input()
{
  uint16_t cur_x = current_active_x;
  uint16_t cur_y = current_active_y;
  bool moved = false;
  
  // if we are on a boxes, toggle on center press, or move u/d/r/l
  if((cur_x < 8) && (cur_y < 8)) {
    if(b_b.uniquePress()) {
      notes[cur_y][cur_x] = !notes[cur_y][cur_x];
      tft.fillRect(cur_x*16, cur_y*16, 16, 16, notes[cur_y][cur_x]?ILI9163_WHITE:ILI9163_BLACK);
    }else
    if(b_left.uniquePress()) {
      if(cur_x > 0) {
        cur_x -= 1;
      }
      else {
        cur_x = 7;
      }
      moved = true;
    }else 
    if(b_right.uniquePress()) {
      if(cur_x < 7) {
        cur_x += 1;
      }
      else {
        cur_x = 0;
      }
      moved = true;
    }else 
    if(b_up.uniquePress()) {
      if(cur_y > 0) {
        cur_y -= 1;
      }
      else {
        cur_y = 7;
      }
      moved = true;
    }else 
    if(b_down.uniquePress()) {
      if(cur_y < 7) {
        cur_y += 1;
      }
      else {
        cur_y = 0;
      }
      moved = true;
    }

    if(moved) {
      tft.drawRect(cur_x*16, cur_y*16, 16, 16, ILI9163_RED);
      tft.drawRect(current_active_x*16, current_active_y*16, 16, 16, ILI9163_WHITE);
      current_active_x = cur_x;
      current_active_y = cur_y;
    }
  }
}