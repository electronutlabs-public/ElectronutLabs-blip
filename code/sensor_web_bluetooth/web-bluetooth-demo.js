window.tdiff = []; fred = function (a, b) { return a - b; };
var indriumTX;
protobuf.load("indrium.proto", function (err, root) {
    if (err)
        throw err;
    indriumTX = root.lookupType("indrium.IndriumTx");
});
window.onload = function (e) {

    var gouter = document.getElementById("graphOuter");
    var canvas = document.createElement('canvas');

    canvas.id = "mycanvas";
    canvas.width = gouter.offsetWidth;
    canvas.height = 125;
    canvas.style.zIndex = 8;
    // canvas.style.position = "absolute";
    // canvas.style.border = "1px solid";
    gouter.appendChild(canvas);

    var smoothie = new SmoothieChart({ millisPerPixel: 5, minValue: -20, maxValue: 20, maxValueScale: 1.1, minValueScale: 1.1, grid: { fillStyle: '#ffffff', strokeStyle: '#00a2e4' }, labels: { fillStyle: '#000000' } });
    var line1 = new TimeSeries();
    var line2 = new TimeSeries();
    var line3 = new TimeSeries();
    smoothie.streamTo(document.getElementById("mycanvas"), 500);

    // Add to SmoothieChart
    smoothie.addTimeSeries(line1, { strokeStyle: 'rgb(0, 255, 0)', lineWidth: 3 });
    smoothie.addTimeSeries(line2, { strokeStyle: 'rgb(255, 0, 0)', lineWidth: 3 });
    smoothie.addTimeSeries(line3, { strokeStyle: 'rgb(0, 0, 255)', lineWidth: 3 });


    var Writecharacteristic;
    var connect = document.getElementById("connect");
    var temp = document.getElementById("temp");
    var humidity = document.getElementById("humidity");
    var lux = document.getElementById("lux");
    connect.addEventListener('click', function (event) {
        navigator.bluetooth.requestDevice({
            acceptAllDevices: true,
            optionalServices: ['def9bda9-4370-469c-98d0-7986a8b64034']
        })
            .then(device => {
                // Human-readable name of the device.
                console.log('Connecting to GATT Server...' + device.name);
                // Attempts to connect to remote GATT Server.
                return device.gatt.connect();
            })
            .then(server => {
             
                console.log('Getting Services...');
                return server.getPrimaryService('def9bda9-4370-469c-98d0-7986a8b64034');
            })
            .then(service => {
                service.getCharacteristics().then(characteristics => {
                    console.log(characteristics);

                    console.log('> Service: ' + service.uuid);
                    console.log(characteristics[0].uuid)

                    var TXcharacteristic = characteristics[0];
                    TXcharacteristic.startNotifications().then(_ => {
                        console.log('> Notifications started');
                        TXcharacteristic.addEventListener('characteristicvaluechanged',
                            processBuff);

                    });
                })
            })
            .catch(error => {
                console.log('Argh! ' + error);
            });
    });

    var finalData;
    var gx, gy, gz;
     function processBuff(event) {
        var data = event.target.value.buffer

        var array = Array.from(new Uint8Array(data))
        if (array[1] == 0 && array[0] == 1) {
            finalData = array.slice(2);
        } else {
            if (!finalData) {
                finalData = [];
            }
            finalData = finalData.concat(array.slice(2))
        }
        if (new Uint8Array(data)[1] == 0 && new Uint8Array(data)[0] == 0) {

            var message = indriumTX.decode(finalData);

            var obj = indriumTX.toObject(message, {
                enums: String,
                longs: String,
                bytes: String,
                defaults: true,
                arrays: true,
                objects: true,
                oneofs: true
            });

            console.log(obj);

            if (obj.sensor) {
                for (let i = 0; i < obj.sensor.length; i++) {
                    const sensor = obj.sensor[i];
                    if (sensor.type == "TYPE_TEMPERATURE") {
                        temp.innerHTML = (sensor.tInt || sensor.tFloat).toFixed(2);
                    } else if (sensor.type == "TYPE_BRIGHTNESS") {
                        lux.innerHTML = (sensor.tInt || sensor.tFloat).toFixed(2);
                    } else if (sensor.type == "TYPE_RELATIVE_HUMIDITY") {
                        humidity.innerHTML = (sensor.tInt || sensor.tFloat).toFixed(2);
                    } else if (sensor.type == "TYPE_ACCELERATION") {
                        if (sensor.instanceID == 1) {
                            gx = (sensor.tInt || sensor.tFloat).toFixed(2);
                        } else if (sensor.instanceID == 2) {
                            gy = (sensor.tInt || sensor.tFloat).toFixed(2);
                        } else {
                            gz = (sensor.tInt || sensor.tFloat).toFixed(2);
                        }

                        line1.append(new Date().getTime(), gx);
                        line3.append(new Date().getTime(), gz);
                        line2.append(new Date().getTime(), gy);
                    }
                }
            }

        }


    }
}
