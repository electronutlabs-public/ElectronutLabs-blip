## What is CircuitPython?

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/VOls-ZQ6Rgw?start=10" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

CircuitPython is a programming language based on Python, and it
greatly simplifies the process of learning and experimenting with
a microcontroller. Once you have Circuitpython running on your Blip,
you don't need to install anything on your computer, just you connect
its peripheral USB port (not the programming one), and it opens up as
a disk drive "CIRCUITPY". All you need now is a text editor!

Python (and hence CircuitPython) is really easy to understand and
learn, so even if you don't know python yet, or don't know much of
programming, you can get started with doing things with Blip using
the following resources and sample code in our repositories.

But, before that, here are the instructions on how to install
CircuitPython on your Blip.

## Installation instructions

### Getting the firmware

There are two ways, you can either download a pre-built binary from CircuitPython
website or compile it yourself. For now, please download the following two files:

* [Nordic semicondictor's s140 softdevice](code/s140/s140_nrf52_6.1.0_softdevice.hex)

* [Circuitpython firmware](code/circuitpython/circuitpython.hex)

### Flashing Circuitpython to Blip

Blip has an onboard programmer and debugger, so we'll just connect the Black Magic
Probe USB, and run `arm-none-eabi-gdb`.

```bash
tavish@computer:~/repos/el/public/ElectronutLabs-blip (master)*$ arm-none-eabi-gdb
GNU gdb (GNU Tools for Arm Embedded Processors 7-2018-q3-update) 8.1.0.20180315-git
Copyright (C) 2018 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
(gdb) tar ext /dev/ttyACM0
Remote debugging using /dev/ttyACM0
warning: No executable has been specified and target does not support
determining executable automatically.  Try using the "file" command.
0x0000384c in ?? ()
(gdb) mon swdp_scan
Target voltage: unknown
Available Targets:
No. Att Driver
 1      Nordic nRF52
 2      Nordic nRF52 Access Port
(gdb) att 1
A program is being debugged already.  Kill it? (y or n) y
Attaching to Remote target
warning: No executable has been specified and target does not support
determining executable automatically.  Try using the "file" command.
0x0000384c in ?? ()
(gdb) load s140_nrf52_6.1.0_softdevice.hex
...
...
(gdb) load s140_nrf52_6.1.0_softdevice.hex
...
...
```

### Learning resources for CircuitPython

* [Welcome to Circuitpython](https://learn.adafruit.com/welcome-to-circuitpython) on [Adafruit](https://learn.adafruit.com) Learning System

* [Circuitpython Essentials](https://learn.adafruit.com/circuitpython-essentials) on [Adafruit](https://learn.adafruit.com) Learning System, and corresponding [examples](https://github.com/adafruit/Adafruit_Learning_System_Guides/tree/master/CircuitPython_Essentials)

## Acknowledgements

Circuitpython is based on the amazing work of [Damien George and the
MicroPython community](https://micropython.org/). A big thank you
to [Adafruit](https://www.adafruit.com/), who created and are funding the
development [CircuitPython](https://circuitpython.org/). And lastly,
thanks for all the CircuitPython community members for the excellent
and beginner friendly CircuitPython ecosystem.
